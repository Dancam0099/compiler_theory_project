#ifndef CT_ASSIGNMENT_MINILANGI_H
#define CT_ASSIGNMENT_MINILANGI_H

#include "Includer.h"
#include "visitor/symbol_table.h"
#include "lexer/lexer.h"
#include "parser/parser.h"
#include "visitor/InterpreterVisitor.h"

class miniLangI {

    SymbolTable *st = new SymbolTable();
    lexer::Lexer *lexer = new lexer::Lexer();
    parser::Parser *parser = new parser::Parser();
    SemanticVisitor *semanticVisitor = new SemanticVisitor();
    InterpreterVisitor *interpreterVisitor = new InterpreterVisitor();
    vector<func> functions;
    vector<AstFunctionNode *> funcnodes;
    string validate_output(string ans);
public:
    void SingleCommand(string command); // handles single commands.
    void help();//all possible commands
    void load(string command); // handles program execution
    void reset(); // resets all internal objects
    void stPrint(); // prints symbol table
    ~miniLangI();


};


#endif //CT_ASSIGNMENT_MINILANGI_H
