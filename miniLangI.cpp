
#include <tic.h>
#include <sstream>
#include <iterator>
#include <cstring>
#include <iomanip>
#include "miniLangI.h"


int main(){
    string command;
    bool quit = false;
    auto *langI = new miniLangI();
    langI->help();
    while(!quit){
        cout<<"MiniLangI> ";
        getline(cin,command);

        if(command == "#quit"){
            cout << "Exiting REPL..."<<endl;
            quit = true;
        }else if(command == "#help"){
            langI->help();
        }else if(command == "#st"){
            langI->stPrint();
        }else if(command.substr(0,5) == "#load"){
            langI->load(command);
        }else{
            langI->SingleCommand(command);
        }
        command.clear();
    }
    delete langI;
}

void miniLangI::SingleCommand(string command) {
    lexer->line = command;
    lexer->isCommand = true;
    lexer->linenum = 0;
    if(!parser->start_parser(lexer))
        return;
    if(semanticVisitor->initiateSemantic(parser->getSyntaxTree(),st,functions)){
        interpreterVisitor->initiateInterpreter(st,parser->getSyntaxTree(),functions,funcnodes);

        if(command.find("print")==std::string::npos){
            cout<<"Val ans : "<<validate_output(interpreterVisitor->curr_val)<<" = "<<interpreterVisitor->curr_val<<endl;
        }
    }
    reset();

}



string miniLangI::validate_output(string ans) {
    if(lexer::check_digit(ans)) return "int";
    if(lexer::checkfloat(ans)) return "real";
    if(ans == "false" || ans == "true") return "bool";
    return "string";
}


void miniLangI::help() {
    cout<<"========================="<<endl;
    cout<<"AVAILABLE COMMANDS"<<endl;
    cout<<"========================="<<endl;
    cout<<"#st   - prints symbol table"<<endl;
    cout<<"#load - load external programs"<<endl;
    cout<<"#help  - shows all commands available"<<endl;
    cout<<"#quit  - exits the terminal"<<endl;
    cout<<"========================="<<endl;
}

vector<string> split(const char *command, char c = ' ')//token spliter
{
    vector<string> result;

    do
    {
        const char *begin = command;

        while(*command != c && *command)
            command++;

        result.push_back(string(begin, command));
    } while (0 != *command++);

    return result;
}
void miniLangI::load(string command) {
    string filename;
    auto p = new char[command.length()+1];
    memcpy(p, command.c_str(), command.length()+1);
    vector<string> result = split(p);
    if(result.size() <= 1 ||result.size() > 2){
        cout<<"not enough or too much parameters"<<endl;

    }
    filename = result[1];
    if(!parser->start_parser(filename))
        return;
    if(!semanticVisitor->initiateSemantic(parser->getSyntaxTree(),st))
        return;

    for (int i = 0; i < semanticVisitor->getfunctionNodes().size(); ++i) {
        funcnodes.push_back(semanticVisitor->getfunctionNodes()[i]);
    }
    for (int j = 0; j < semanticVisitor->getFunctions().size(); ++j) {
        functions.push_back(semanticVisitor->getFunctions()[j]);
    }
    interpreterVisitor->initiateInterpreter(st,parser->getSyntaxTree(),semanticVisitor->getFunctions());
    reset();
}

void miniLangI::reset() {
    semanticVisitor = new SemanticVisitor();
    parser = new parser::Parser();
    lexer = new Lexer();
    interpreterVisitor = new InterpreterVisitor();
}

void miniLangI::stPrint() {
    cout<<"========================="<<endl;
    cout<<"SYMBOL TABLE"<<endl;
    cout<<"========================="<<endl;
    for (auto &i : st->global) {
        cout<<"Identifier: "<<get<0>(i)<<endl;
        cout<<"Type: "<<get<1>(i)<<endl;
        cout<<"Value: "<<get<2>(i)<<endl;
        cout<<"========================="<<endl;
    }

}

miniLangI::~miniLangI() {
    delete  st;
    delete lexer;
    delete parser;
    delete semanticVisitor;
    delete interpreterVisitor;
    for (int i = 0; i < funcnodes.size(); ++i) {
        delete funcnodes[i];
        funcnodes.pop_back();
    }
}
