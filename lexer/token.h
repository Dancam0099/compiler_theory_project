
#ifndef CT_ASSIGNMENT_TOKEN_H
#define CT_ASSIGNMENT_TOKEN_H

#include "../Includer.h"
using namespace std;
namespace lexer{
    enum Token_Type{ // ALL TOKEN TYPES
        TOK_NUM,
        TOK_FLT,
        TOK_ARITHMETICOP,
        TOK_MULTIOP,
        TOK_EOF,
        TOK_ERR,
        TOK_SEMIC,
        TOK_COLON,
        TOK_COMMA,
        TOK_RELOP,
        TOK_oBRACKET,
        TOK_cBRACKET,
        TOK_DOUBQUOTE,
        TOK_oSQBRACKET,
        TOK_cSQBRACKET,
        TOK_EQUALS,
        TOK_IDENTIFIER,

        /*IDENTIFIER TYPES*/
        TOK_REAL,
        TOK_INT,
        TOK_BOOL,
        TOK_STRING,

        /*BOLEAN TPYES*/
        TOK_TRUE,
        TOK_FALSE,

        /*UNARY AND OR*/
        TOK_UOR,
        TOK_AND,
        TOK_NOT,

        /*RETURN, IFS AND LOOPS*/
        TOK_RETURN,
        TOK_IF,
        TOK_ELSE,
        TOK_WHILE,

        /*VAR ASSIGNMENT, PRINT AND FUNCTION DEF*/
        TOK_SET,
        TOK_VAR,
        TOK_PRINT,
        TOK_FUNCDEF,
        TOK_STRLITERAL,
        TOK_WHITESPACE,

        COMMENT,
        oBLOCKC,
        cBLOCKC,
        TOK_EMPTY_LINE

    };

    // stores the token type and value of the current token.
    struct token{
        Token_Type type;
        string value;

    };

    /*
     * Checks all characters to ensure its a whole number.
     *
     * @param lexeme - current string to check
     * */
    bool check_digit(string lexeme);
    /*
     * Checks all charaters to ensure its a string.
     *
     * @param lexeme - current string to check
     * */
    bool isalphabet(string lexeme);

    /*
     * Checks all characters to ensure its a decimal number.
     *
     * @param lexeme - current string to check
     * */
    bool checkfloat(string lexeme);
    /*
     * Verifies the lexeme to make sure its valid
     *
     * @param lexeme - current string to check
     *
     * @return token value of current lexeme
     * */
    Token_Type tok_check(string lexeme);
}

#endif //CT_ASSIGNMENT_TOKEN_H
