#include <sstream>
#include <algorithm>
#include "token.h"
#include "lexer.h"


bool lexer::check_digit(string lexeme) {
    int i = 0;
    while(lexeme[i] != '\0'){
        if(!isdigit(lexeme[i]))
            return false;
        i++;
    }
    return true;
}

bool lexer::isalphabet(string lexeme) {
    for (char i : lexeme) {
        if(!isalpha(i))
            return false;
    }
    return true;
}
bool lexer::checkfloat(string lexeme) {
    for (char i : lexeme) {
        if (!isdigit(i) && i != '.')
            return false;
    }

    return true;
}

lexer::Token_Type lexer::tok_check(string lexeme) {
    if(lexeme.empty()) return TOK_ERR;
    if(lexeme[0] == '/' && lexeme[1] == '/' ) return COMMENT;
    if(lexeme[0] == '"' && lexeme[lexeme.size()-1] == '"'){
        return TOK_STRLITERAL;
    }

    if(lexeme.find('.') != std::string::npos && checkfloat(lexeme))
        return TOK_FLT;

    if(check_digit(lexeme))
        return TOK_NUM;

    if(lexeme[1] == '\0'){
        switch(lexeme[0]){
            case '+':
            case '-':
                return TOK_ARITHMETICOP;
            case '/':
            case '*':
                return  TOK_MULTIOP;
            case ';':
                return TOK_SEMIC;
            case ':':
                return TOK_COLON;
            case ',':
                return TOK_COMMA;
            case '>':
                return TOK_RELOP;
            case '<':
                return TOK_RELOP;
            case '"':
                return TOK_DOUBQUOTE;
            case '(':
                return TOK_oBRACKET;
            case ')':
                return TOK_cBRACKET;
            case '{':
                return TOK_oSQBRACKET;
            case '}':
                return TOK_cSQBRACKET;
            case '=':
                return TOK_EQUALS;
            default:break;
        }
    }

    if(lexeme == ">=") return TOK_RELOP;
    if(lexeme == "<=") return TOK_RELOP;
    if(lexeme == "!=") return TOK_RELOP;
    if(lexeme == "==") return TOK_RELOP;
    if(lexeme == "/*") return oBLOCKC;
    if(lexeme == "*/") return cBLOCKC;
    bool check_white = all_of(lexeme.begin(),lexeme.end(),::isspace);
    if(check_white) return TOK_WHITESPACE;

    if(isalphabet(lexeme)){
        if(lexeme == "true") return TOK_TRUE;
        if(lexeme == "false") return TOK_FALSE;
        if(lexeme == "if") return TOK_IF;
        if(lexeme == "else") return TOK_ELSE;
        if(lexeme == "def") return TOK_FUNCDEF;
        if(lexeme == "int") return TOK_INT;
        if(lexeme == "real") return TOK_REAL;
        if(lexeme == "print") return TOK_PRINT;
        if(lexeme == "var") return TOK_VAR;
        if(lexeme == "set") return TOK_SET;
        if(lexeme == "while") return TOK_WHILE;
        if(lexeme == "string") return TOK_STRING;
        if(lexeme == "bool") return TOK_BOOL;
        if(lexeme == "and") return TOK_AND;
        if(lexeme == "or") return TOK_UOR;
        if(lexeme == "not") return TOK_NOT;
        if(lexeme == "return") return TOK_RETURN;
    }


    if(lexeme.find('_') != std::string::npos && lexeme.length() > 1 || isalphabet(lexeme) || lexeme == "_" || isalpha(lexeme[0])){
        return TOK_IDENTIFIER;
    }
    return TOK_ERR;
}