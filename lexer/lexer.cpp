#include "lexer.h"
#include <algorithm>
bool lexer::Lexer::read_file() {
    if(!langfile.is_open()){
        cout<<"ERROR OPENING FILE"<<endl;
        return false;
    }else{
        cout<<"SUCCESSFUL OPENING FILE"<<endl;
        return true;
    }
}

int lexer::Lexer::NextToken(string line, int start_pt) {
    STATES st = S00; // sets current state to first state
    string lexeme;
    stack<STATES> stack1;
    stack1.push(bad);
    char let = ' '; // random character initialization.
    while(st != S0E && let != '\0'){
        let = line[start_pt];
        if(let == '\t'){ // ensures tabs are not counted
            start_pt+=1;
            continue;
        }

        if (check_final(st)){ // checks if final state
            while(!stack1.empty()){
                stack1.pop();
            }
        }
        stack1.push(st);


        st = transition_table[check_char(let,st)][st]; //gets transition state from table
        if(st != S0E){
            start_pt +=1;
            lexeme += let;
        }else if(let == ' '){
            lexeme += let;
        }
        if(lexeme == "/*" || lexeme == "*/")
            break;
    }

    if(lexeme == "*/" && !isblock){ // block comment error
        tk.type = TOK_ERR;
        tk.value = lexeme;
        return ++start_pt;
    }

    if(!all_of(lexeme.begin(),lexeme.end(),::isspace) && lexeme[0]!= '"') // removes extra spaces
        lexeme.erase(std::remove(lexeme.begin(), lexeme.end(), ' '), lexeme.end());

    if(lexeme.empty()){ // for empty lines
        tk.type = TOK_EMPTY_LINE;
        tk.value = lexeme;
        return start_pt;
    }
    while(!check_final(stack1.top()) && stack1.top() != bad){
        stack1.pop();
        lexeme = lexeme.substr(0,lexeme.size()-1);
    }


    tk.type = tok_check(lexeme);
    tk.value = lexeme;
    return start_pt;
}

void lexer::Lexer::tokens_creator() {

    if(isCommand){
        if(line[linenum] == '\0'){
            tk.type = TOK_EOF;
            return;
        }

        linenum = NextToken(line,linenum);
        if(tk.type == TOK_WHITESPACE)
            tokens_creator();
        return;
    }
    if(line[linenum] == '\0' || line.empty()){
        linenum = 0;
        get_line_(line);
    }

    if(iseof){
        langfile.close();
        tk.type = TOK_EOF;
        tk.value = "";
        return;
    }

    linenum = NextToken(line,linenum);
    if(tk.type == oBLOCKC){
        rem_block_comments(linenum);

        if(tk.type == cBLOCKC)
            tokens_creator();
    }

    if(tk.type == COMMENT){
        linenum = 0;
        get_line_(line);
        tokens_creator();
    }


    if(tk.type == TOK_WHITESPACE || tk.type == TOK_EMPTY_LINE)
        tokens_creator();



}



bool lexer::Lexer::check_final(STATES sts) {
    for (auto &i : fl) {
        if(i == sts)
            return true;
    }
    return false;
}




lexer::Lexer::Lexer(){
    tk.value = "";
    tk.type = TOK_WHITESPACE;
}

lexer::Lexer::Lexer(string nameFile) : langfile(nameFile){
    tk.value = "";
    tk.type = TOK_WHITESPACE;
}

void lexer::Lexer::get_line_(string &line) {
    if(!getline(langfile,line)){
        line  = string();
        iseof = true;

    }

}

void lexer::Lexer::rem_block_comments(int &linenum) {
    isblock = true;
    while(tk.type != cBLOCKC && tk.type != TOK_EOF){
        if(line[linenum] == '\0'){
            linenum = 0;
            get_line_(line);
        }

        if(iseof){
            tk.type = TOK_EOF;
            tk.value = "";

        }

        linenum = NextToken(line,linenum);

    }
    isblock = false;
}