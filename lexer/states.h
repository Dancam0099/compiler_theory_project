#ifndef CT_ASSIGNMENT_STATES_H
#define CT_ASSIGNMENT_STATES_H
namespace lexer{
    enum STATES{ // states used for transition table
        S00 = 0,
        S01 = 1,
        S02 = 2,
        S03 = 3,
        S04 = 4,
        S05 = 5,
        S06 = 6,
        S07 = 7,
        S08 = 8,
        S09 = 9,
        S10 = 10,
        S11 = 11,
        S13 = 12,
        S14 = 13,
        S15 = 14,
        S16 = 15,
        S17 = 16,
        S18 = 17,
        S19 = 18,
        S0E,
        bad,
    };

    /*
     * Checks the current character value and matches it with a state
     *
     *
     * @param letter - character to check
     * @param states - current state
     *
     * @return - value to transition to the next state.
     *
     * */
    int check_char(char letter,STATES states);


}
#endif //CT_ASSIGNMENT_STATES_H
