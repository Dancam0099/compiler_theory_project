#include "states.h"

int lexer::check_char(char letter,STATES state) {

    auto num  = (int)letter;
    if((state == 15 || state == 13) && (num >=32 && num <= 126) && letter != '"' ){
        return 14;
    }


    if(state == 14){
        return 16;
    }


    if(((num >=65 && num <= 90 ) || (num >= 97 && num <= 122))){
        return 6;
    }
    switch (letter){
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if(state == 0 || state == 1 )
                return 1;
            else if(state == 3 || state == 2)
                return 3;
            else if(state == 17 || state == 5 || state == 12)
                return 19;
        case '.':
            return 2;
        case '+':
        case '-':
            return 4;
        case '/':
        case '*':
            if(state == 0)
                return 4;
            else if(state == 4)
                return 15;
        case '\0':
            return 5;
        case ';':
        case ':':
        case ',':
            return 7;
        case '<':
        case '>':
            return 8;
        case '!':
            return 9;
        case '=':
            if(state == 0)
                return 10;
            else if(state == 7 || state == 8 || state == 9)
                return 11;
        case '"':
            if(state == 0)
                return 17;
            else if(state == 13 || state == 15)
                return 18;
        case '{':
        case '}':
        case '(':
        case ')':
            return 12;
        case '_':
            return 13;
        case ' ':
            return 20;
        default:
            return 5;
    }
}