#ifndef CT_ASSIGNMENT_LEXER_H
#define CT_ASSIGNMENT_LEXER_H
#include "../Includer.h"
#include "token.h"
#include "states.h"

using namespace std;

namespace lexer{

    class Lexer {
    public:
        /*
         * Constructor mainly used in parser to parse single commands
         * from REPL.
         *
         */
        Lexer();

        /*
         * Constructor used in parser to parse entire files of type txt
         * Input files are received trough the REPL.
         *
         * @param nameFile - name of the file
         */
        Lexer(string nameFile);

        /*
         * Reads the file requested by the user mainly used in parser.
         *
         *
         * @return true - if file exist || false - if file does not exist
         * */
        bool read_file();

        /*
         * Creates tokens from a string one at a time.
         * By using an the integer linenum it allows the method to keep track of the current character.
         *
         * */
        void tokens_creator();

        /*
         * Creates a token by using a stack to store states and a transition table to ensure
         * that only valid tokens are created and passed trough the parser.
         *
         * @param line - the current string line to split into tokens
         * @param start_pt - the starting point character  to start evaluating the token.
         *
         * @return returns current position of the string
         * */
        int NextToken(string line, int start_pt);

        /*
         * Checks if the current states is a final state.
         *
         *
         * @param sts - the current state
         *
         * @return true - if the current state is final || false - if the current state is not final
         * */
        bool check_final(STATES sts);

        /*
         * Gets the next line from the given file.
         *
         * @param line - line from file stored in this parameter.
         * */
        void get_line_(string &line);

        /*
         * processes block comments
         *
         * @param linenum - the current position in the string to start processing block comment
         * */
        void rem_block_comments(int &linenum);


        int linenum = 0; // current line value
        token tk; // stores token type and value
        ifstream langfile; // open file stream
        string line; // current line to parse
        bool isblock = false; // checks if block comment
        bool iseof = false; // checks if eof
        bool isCommand = false; // checks if single command

        // transition table
        STATES transition_table[21][19] = {        {S00,S01,S02,S03,S04,S05,S06,S07,S08,S09,S10,S11,S13,S14,S15,S16,S17,S18,S19},
                                         /*DIGITS*/{S01,S01,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                        /*DECIMAL*/{S0E,S02,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                           /*DIGITS AFTER DECIMAL*/{S0E,S0E,S03,S03,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                  /*ARITHMETIC OP*/{S04,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                           /*EOF */{S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                       /*ALPHABET*/{S05,S0E,S0E,S0E,S0E,S05,S0E,S0E,S0E,S0E,S0E,S0E,S05,S0E,S0E,S0E,S0E,S05,S0E},
                              /*COMMAS AND COLONS*/{S06,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                      /*GREATER THAN OR LESS THAN*/{S07,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                               /*NOT EQUAL SYMBOL*/{S08,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                         /*EQUALS*/{S09,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                      /*EQUALS TO*/{S0E,S0E,S0E,S0E,S0E,S0E,S0E,S10,S10,S10,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                      /*BRACKETS */{S11,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                     /*UNDERSCORE*/{S13,S0E,S0E,S0E,S0E,S13,S0E,S0E,S0E,S0E,S0E,S0E,S13,S0E,S0E,S0E,S0E,S13,S0E},
                                     /*PRINTABLES*/{S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S14,S0E,S14,S0E,S0E,S0E},
                                      /*BACKSLASH*/{S0E,S0E,S0E,S0E,S15,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                     /*EVERYTHING*/{S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S15,S0E,S0E,S0E,S0E},
                       /*DOUBLE QUOTES FROM START*/{S16,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E},
                                  /*DOUBLE QUOTES*/{S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S17,S0E,S17,S0E,S0E,S0E},
                              /*IDENTIFIER DIGITS*/{S0E,S0E,S0E,S0E,S0E,S18,S0E,S0E,S0E,S0E,S0E,S0E,S18,S0E,S0E,S0E,S0E,S18,S0E},
                                    /*WHITESPACES*/{S19,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S0E,S19}};
        STATES fl[14] = {S01,S03,S04,S05,S06,S07,S09,S10,S11,S13,S15,S17,S18,S19};

    };

}
#endif //CT_ASSIGNMENT_LEXER_H
