def funcPow ( x : real , n : int ) : real{
    var y : real = 1.0;
    if(n>0){
        while(n>0)
        {
        set y = y * x;
        set n = n - 1;
        }
    }
    else{

        while(n<0){
            set y = y / x;
            set n = n + 1;
        }
    }
    return y;
}

var y : int = 35;

if(funcPow(5,3) == 100){
   set y = 10;
}else{ 
   set y = 50;
}

print y;

