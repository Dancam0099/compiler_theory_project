def square(x : real) : real{
       return x*x;
}
def fib(limit : int) : string{
    var t1: int = 0;
    var n: int = 1;
    var t2: int = 1;
    var nextTerm: int = 0;
    print "-------------------";
    print "fibonacci Sequence";
    print "-------------------";
    while(n <= limit){
     if(n == 1){
        print t1;
     }else{
        if(n == 2){
              print t2;

          }else{
              set nextTerm = t1 + t2;
              set t1 = t2;
              set t2 = nextTerm;
              print nextTerm;
          }
     }
     set n = n + 1;
    }
    return "done";
}

def calculator(x : real , n : real, s: string): real{
	if(s == "add"){
		return x+n;
	}
	if(s == "sub"){
		return x-n;
	}

	if(s == "mul"){
		return x*n;
	}

	if(s == "div"){
		return x/n;
	}
}

var n : int = 0;
var x : int = 0;
while(n <= 100){
 set x = x + calculator(3,10,"add");
 set n = n+1;
}


print x;
print n;
print square(4+4);
print calculator(-3,-10,"add");
