//
// Created by daniel on 4/29/18.
//

#include "symbol_table.h"

void SymbolTable::new_scope(Scope *scope) {
    scopeIn = scope;
}

void SymbolTable::insert_in_scope(Scope *scp, int start, int stop, tuple<string, string, string> variable) {
    if(start==stop){
        scp->variables.push_back(variable);
    }else{
        if(scp->scope == nullptr)
            scp->scope = new Scope();
        insert_in_scope(scp->scope,++start,stop,variable);
    }

}

void SymbolTable::remove_scope(Scope *scp, int start, int stop) {
    Scope *prev;
    while(start != stop){
        if(scp == nullptr)
            return;

        prev = scp;
        if(scp->scope == nullptr)
            return;
        scp = scp->scope;
        start++;

    }
    delete scp;
    if(stop != 0)
        prev->scope = nullptr;
}

bool SymbolTable::searchInScope(Scope *scope, string var) {
    if(scope == nullptr)
        return false;
    for (auto &variable : scope->variables) {
        if(get<0>(variable) == var){
            return true;
        }
    }
    if(scope->scope != nullptr){
        return searchInScope(scope->scope,var);
    }

    return false;

}

string SymbolTable::scopeTypeCheck(Scope *scope, string var) {
    for (auto &variable : scope->variables) {
        if(get<0>(variable) == var){
            return get<1>(variable);
        }
    }
    if(scope->scope != nullptr){
        return scopeTypeCheck(scope->scope,var);
    }else return "null";

}

string SymbolTable::typeCheck(lexer::Token_Type tokenType) {
    switch(tokenType){
        case lexer::TOK_INT:
            return "int";
        case lexer::TOK_REAL:
            return "real";
        case lexer::TOK_BOOL:
            return "bool";
        case lexer::TOK_STRLITERAL:
            return "string";
        default:
            return "null";
    }

}

string SymbolTable::getScopeValue(Scope *scope, string var) {
    for (auto &variable : scope->variables) {
        if(get<0>(variable) == var){
            return get<2>(variable);
        }
    }
    if(scope->scope != nullptr){
        return getScopeValue(scope->scope,var);
    }
}

void SymbolTable::setValue(Scope *scope, string var, string value) {
    for (auto &variable : scope->variables) {
        if(get<0>(variable) == var){
            get<2>(variable) = value;
            return;
        }
    }
    setValue(scope->scope,var,value);

}

void SymbolTable::createScope(Scope *scp, int start, int stop) {
    Scope *prev;
    while(start != stop){
        if(scp == nullptr)
            return;

        prev = scp;
        if(scp->scope == nullptr)
            return;
        scp = scp->scope;
        start++;

    }
    delete scp;
    prev->scope = new Scope();
}

SymbolTable::~SymbolTable() {
    delete scopeIn;
}

Scope::~Scope() {
    delete scope;
}
