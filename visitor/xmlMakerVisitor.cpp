#include "xmlMakerVisitor.h"
#include "../parser/ast_binary_expression_node.h"
#include "../parser/ast_var_declare_statement.h"
#include "../parser/ast_assignment_statement.h"
#include "../parser/ast_formalparams.h"
#include "../parser/ast_funcDec.h"
#include "../parser/ast_function_call_expression_node.h"
#include "../parser/ast_while_statement.h"
#include "../parser/Ast_Unary_Expression_node.h"
#include "../parser/ast_return_statement.h"
#include "../parser/ast_print_statement.h"
#include "../parser/ast_if_statement.h"

void XmlMakerVisitor::visit(AstWhileStatementNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<While>"<<endl;
    indent+=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<While_Param>"<<endl;
    indent+=1;
    node->params->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</While_Param>"<<endl;
    xmlfile<<tabs<<"<While_Statements>"<<endl;
    indent+=1;
    node->while_statements->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</While_Statements>"<<endl;
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</While>"<<endl;
}


void XmlMakerVisitor::visit(AstVarDeclareStatement *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<Decl>"<<endl;
    indent+=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile <<tabs<<"<Id Type ="<<"\""<<typeCheck(node->type)<<"\""<<">"<<node->identifier<<"</Id> "<<endl;
    node->value->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs;
    xmlfile <<"</Decl>"<<endl;
}

void XmlMakerVisitor::visit(AstUnaryExpresionNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<Unary>"<<endl;
    indent+=1;
    node->un_exp->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs;
    xmlfile <<"</Unary>"<<endl;
}

void XmlMakerVisitor::visit(AstReturnStatementNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<Return>"<<endl;
    indent+=1;
    node->returnexp->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile <<tabs<<"</Return>"<<endl;

}

void XmlMakerVisitor::visit(AstPrintStatementNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<Print>"<<endl;
    indent+=1;
    node->print->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile <<tabs<<"</Print>"<<endl;
}

void XmlMakerVisitor::visit(AstIfStatementNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<If>"<<endl;
    indent+=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<If_Condition>"<<endl;
    indent+=1;
    node->ifcondition->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</If_Condition>"<<endl;

    xmlfile<<tabs<<"<If_Statements>"<<endl;
    indent+=1;
    node->if_statements->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</If_Statements>"<<endl;
    if(node->else_statements != nullptr){
        xmlfile<<tabs<<"<Else_Statements>"<<endl;
        indent+=1;
        node->else_statements->accept(this);
        indent-=1;
        tabs = "";
        for (int t = 0; t<indent; t++) tabs.append("\t");
        xmlfile<<tabs<<"</Else_Statements>"<<endl;
    }
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</If>"<<endl;
}

void XmlMakerVisitor::visit(AstFunctionCallExpressionNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<Func_call>"<<endl;

    indent+=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile <<tabs<<"<Function_Id>"<<node->identifier<<"</Function_Id> "<<endl;
    xmlfile<<tabs<<"<Actual_Params>"<<endl;
    indent+=1;
    for (auto &i : node->func_expr) {
        i->accept(this);
    }
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</Actual_Params>"<<endl;

    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</Func_call>"<<endl;

}

void XmlMakerVisitor::visit(AstFunctionNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"<Func>"<<endl;

    indent+=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile <<tabs<<"<Function Type ="<<"\""<<typeCheck(node->type)<<"\""<<">"<<node->identifier<<"</Function> "<<endl;
    xmlfile<<tabs<<"<Function_Params>"<<endl;
    indent+=1;
    for (auto &internal_parameter : node->internal_parameters) {
        internal_parameter->accept(this);
    }
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</Function_Params>"<<endl;
    xmlfile<<tabs<<"<Function_Statements>"<<endl;
    indent+=1;
    node->block->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</Function_Statements>"<<endl;

    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</Func>"<<endl;
}

void XmlMakerVisitor::visit(AstBinaryNode *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs;
    if(node->LHS == nullptr &&node->RHS == nullptr){
        if(checkfloat(node->operator_)){
            xmlfile<<"<real>"<<node->operator_<<"</real>"<<endl;
        }else if(check_digit(node->operator_)) {
            xmlfile<<"<int>"<<node->operator_<<"</int>"<<endl;
        } else if(node->operator_ == "true" || node->operator_ == "false")
            xmlfile<<"<bool>"<<node->operator_<<"</bool>"<<endl;
        else
            xmlfile<<"<string>"<<node->operator_<<"</string>"<<endl;

        return;
    }
    xmlfile<<"<BinExprNode Op = \""<<node->operator_<<"\">"<<endl;
    indent+=1;
    node->LHS->accept(this);
    if(node->RHS != nullptr)
        node->RHS->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs<<"</BinExprNode>"<<endl;

}

XmlMakerVisitor::XmlMakerVisitor() :xmlfile("syntaxTree.xml"),indent(0){}

void XmlMakerVisitor::visit(ASTFormalParams *node) {
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile <<tabs<<"<Id Type ="<<"\""<<typeCheck(node->type)<<"\""<<">"<<node->identifier<<"</Id> "<<endl;

}

void XmlMakerVisitor::visit(AstBlockStatementNode *node) {
    for (auto &b_statement : node->b_statements) {
        b_statement->accept(this);
    }
}

void XmlMakerVisitor::visit(AstAssignmentStatementNode *node) {
    string tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs;
    xmlfile <<"<Set>"<<endl;
    indent+=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile <<tabs<<"<Id>"<<node->identifier<<"</Id> "<<endl;
    node->value->accept(this);
    indent-=1;
    tabs = "";
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs;
    xmlfile <<"</Set>"<<endl;
}

void XmlMakerVisitor::InitiateXML(vector<AstStatementNode *> syntaxtree) {
    xmlfile <<"<Program>"<<endl;
    indent+=1;

    for (auto &i : syntaxtree) {
        if(i != nullptr)
            i->accept(this);
    }
    indent-=1;
    string tabs;
    for (int t = 0; t<indent; t++) tabs.append("\t");
    xmlfile<<tabs;
    xmlfile <<"</Program>"<<endl;
}

string XmlMakerVisitor::typeCheck(lexer::Token_Type tokenType) {
    switch(tokenType){
        case TOK_INT:
            return "int";
        case TOK_REAL:
            return "real";
        case TOK_BOOL:
            return "bool";
        case TOK_STRLITERAL:
            return "string";
        default:
            return "null";
    }
}

void XmlMakerVisitor::visit(AstFunctionCallStatement *node) {
    node->functionCall->accept(this);
}
