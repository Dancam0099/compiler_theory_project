#ifndef CT_ASSIGNMENT_INTERPRETERVISITOR_H
#define CT_ASSIGNMENT_INTERPRETERVISITOR_H


#include "visitor.h"
#include "symbol_table.h"
#include "../parser/ast_funcDec.h"
#include "../parser/ast_while_statement.h"
#include "../parser/ast_var_declare_statement.h"
#include "../parser/ast_assignment_statement.h"
#include "../parser/ast_if_statement.h"
#include "../parser/ast_function_call_expression_node.h"
#include "../parser/ast_binary_expression_node.h"
#include "../parser/ast_print_statement.h"
#include "../parser/ast_return_statement.h"
#include "../parser/Ast_Unary_Expression_node.h"
#define FLOATINIT DBL_MAX
class InterpreterVisitor: public Visitor {
    SymbolTable *ST = new SymbolTable(); // symbol table object
    Scope *scope = new Scope(); // scope object
    bool isFuncCall = false; // checks if function call
    vector<func> functions; // stores correct functions
    int paramcount = 0; // parameter count for given function
    int subScopeCount = -1; // scope track
    string currentFuncName = ""; // current function name
    bool condition = false; // condition verifier
    string expression = ""; // temp string store
    vector<AstFunctionNode *> funcNodes; // function nodes from syntax tree
    bool isreturn = false; // checks if return
    /*
     * Processes an expression tree storing the result in the variable expression.
     * */
    void visit(AstBinaryNode *node) override;
    /*
     * Processes a function that was called with the given values for each paramter.
     * */
    void visit(AstFunctionNode *node) override;
    /*
     * Processes function call by first adding each expression to the function's struct.
     * afterwards that said function is called. The value of the function struct are cleared after the function
     * has been processed.
     * */
    void visit(AstFunctionCallExpressionNode *node) override;
    /*
     * checks the if expression if it is true then it executes the block statement
     * if the if condition is false the else block statement will executed only if its not null.
     * */
    void visit(AstIfStatementNode *node) override;
    /*
     * executes the expression and prints the result by using cout
     * */
    void visit(AstPrintStatementNode *node) override;
    /*
     * returns a value from a function after the said expression is executed.
     * */
    void visit(AstReturnStatementNode *node) override;
    /*
     * NOT USED
     * */
    void visit(AstUnaryExpresionNode *node) override;
    /*
     * Executes the value of a the variable and stores it in the symbol table.
     * */
    void visit(AstVarDeclareStatement *node) override;
    /*
     * checks while condition if it holds the statements of the while loop are executed.
     * the expression is checked once again after the while is done executing its statements
     * */
    void visit(AstWhileStatementNode *node) override;
    /*
     * Inserts the variables alongside their values in the symbol table
     * */
    void visit(ASTFormalParams *node) override;
    /*
     * Processes block statements then proceeds to deleting them after finish
     * */
    void visit(AstBlockStatementNode *node) override;
    /*
     * executes the expression and updates the variable value in the symbol table.
     * */
    void visit(AstAssignmentStatementNode *node) override;
    /*
     * executes stand alone function calls.
     * */
    void visit(AstFunctionCallStatement *node) override;
    /*
     * converts a token type to a string
     * @return string: converted from token type to string mainly used for literal types
     * */
    string typeCheck(lexer::Token_Type tokenType);

    /*
     * given an identifier this method searches for the function node to execute.
     * @param function: function name.
     * */
    void executeFunction(string function);
    /*
     * returns the value of a variable which is global.
     * @param var: variable to search
     * @return string: value of said variable
     * */
    string valueGlobalCheck(string var);
    /*
     * retrieves the value of the current parameter from the respective function struct
     * to store in the symbol table.
     * @return string: value of function parameter.
     * */
    string parameterValueReturn();
    /*
     * returns the type of a global variable.
     * @param identifier: variable to search for.
     * @return string:
     * */
    string checkGlobalType(string identifier);
    /*
     * returns the value of a variable which is global.
     * @param var: variable to search
     * @return string: value of said variable
     * */
    string getGlobalValue(string identifier);
    /*
     * sets the value for a global variable
     * @param identifier: variable to search
     * */
    void setGlobalValue(string identifier);
    /*
     * executes a given expression by first checking if its a number expression.
     * then checks if its relation and after it checks if its a binary expression.
     * */
    bool expressionIntrepretation(string num1,string num2,string op);
    /*
     * converts numbers to integers, evaluates those numbers and re converts the result to string.
     * */
    void evaluateArtimeticExpression(string num1,string num2,string op);
    /*
     * executes actual expression separating number relation from string and bool relations.
     * */
    void evaluateRelationExpression(string num1,string num2,string op);
    /*
     * executes binary expression.
     * */
    void evaluateBinaryExpression(string num1,string num2,string op);
    /*
    * checks if variable is global
    * @param identifier: checks if variable exists globally
    * @return true: if variable exists, false: if variable does not exist.
    * */
    bool checkGlobal(string identifier);

    bool check_artimetic_op(string op);
    bool check_relation_op(string op);
    bool check_binary_op(string op);
    bool check_digit(string op);
    bool check_float(string op);


    string relToBool(string res); // changes return value of tostring to string boolean
    bool check_if_bool(string num); //checks if number is a boo
    void floatToInt(); //converts a float number to an integer.
public:
    ~InterpreterVisitor();
    string curr_val; // current value to print
    //executes programs
    void initiateInterpreter(SymbolTable *symbolTable,vector<AstStatementNode *> program,vector<func> functions);
    //exectues single line commmands
    void initiateInterpreter(SymbolTable *symbolTable,vector<AstStatementNode *> program,vector<func> functions,vector<AstFunctionNode *> funcNodes);
};


#endif //CT_ASSIGNMENT_INTERPRETERVISITOR_H
