#ifndef CT_ASSIGNMENT_TEST_H
#define CT_ASSIGNMENT_TEST_H


#include <tuple>
#include <vector>
#include "visitor.h"
#include "../lexer/token.h"
#include "symbol_table.h"
#include "InterpreterVisitor.h"
#include <memory>
using namespace std;





class SemanticVisitor : public Visitor{
    SymbolTable *ST = new SymbolTable(); //simple table
    Scope *scope = new Scope(); // scope object to link


    vector<func> functions;//stores functions
    int func_count = 0; // current function counter
    vector<AstFunctionNode *> funcNodes;// stores the functions nodes from sytnax tree


    int sub_scopeC = -1; // sub scope quantity


    bool varExp = false; // global check to see if current expression is for a variable
    bool funCall = false; // global check to see if current expression is for a function call takes a higher precedence than variables
    bool isfunc = false; // checks if its a function call


    string temp_expr = ""; // temporary stores expression results
    string currType = ""; // current type used for function calls and variable expressions
    string currFuncParType = ""; // function parameter type

    /*
     * Extracts the values of an expression/
     * */
    void visit(AstBinaryNode *node) override;
    /*
     * Checks the semantics of a function by  making sure it has a unique identifier, a valid return
     * and also ensures that a function is not declared within a function.
     * */
    void visit(AstFunctionNode *node) override;
    /*
     * Checks if the functions exists globally and matches the amount of parameters and data within those
     * parameters match the ones of the function being called.
     * */
    void visit(AstFunctionCallExpressionNode *node) override;
    /*
     * Validates the expression if it holds then the block statements is checked.
     * if else block statement is not null then it will be checked.
     * */
    void visit(AstIfStatementNode *node) override;
    /*
     * checks expression within the print statement
     * */
    void visit(AstPrintStatementNode *node) override;
    /*
     * checks if return is within a function and also makes sure that the type that is being returns
     * matches with the one of the function
     * */
    void visit(AstReturnStatementNode *node) override;
    /*
     * NOT USED
     * */
    void visit(AstUnaryExpresionNode *node) override;
    /*
     * checks if deceleration is within scope or global.
     * checks that there is no duplication of variable deceleration
     * makes sure binary operators and relation operators are not used.
     * */
    void visit(AstVarDeclareStatement *node) override;
    /*
     * process expression when valid block statements are processed
     * */
    void visit(AstWhileStatementNode *node) override;
    /*
     * makes sure that duplication of parameters does not occur.
     * */
    void visit(ASTFormalParams *node) override;
    /*
     * creates a new scope and process the data of said scope statement by statement
     * after a scope is finished that scope is deleted.
     * */
    void visit(AstBlockStatementNode *node) override;
    /*
     * checks if variable exists within scope or globally(scope has higher precedence than global scope)
     * verifies value to be assigned.
     * */
    void visit(AstAssignmentStatementNode *node) override;
    /*
     * process function call with the statement.
     * */
    void visit(AstFunctionCallStatement *node) override;
    /*
     * checks if variable is global
     * @return true: if variable is global, false: if variable does not exists globally
     * */
    bool checkGlobal(string identifier);
    /*
     * checks the type of the variable value
     * @return true: if type matches, false: if type doesnt match
     * */
    bool varTypeCheck(string num1);
    /*
     * returns the type of a global variable
     * @return string: type of global variable
     * */
    string checkGlobalType(string identifier);
    /*
     * converts the type from enum to string
     * @return string: enum coverted.
     * */
    string typeCheck(lexer::Token_Type tokenType);
    /*
     * checks if number a natural number.
     * @return true: if natural number,false: number is rational or not a number at all
     * */
    bool check_digit(string number);
    /*
     * checks if number is a float number
     * @return true: if number is float, false: if number is not float
     *
     * Keep in mind that if natural numbers are processed through this method they will still return true
     * */
    bool check_float(string number);
    /*
     * verifies the lowest precedence of an expression
     * @return true: if valid expression,false: if expression is invalid
     * */
    bool expressionVerify(string num1,string num2,string op);
    /*
     * verifies the identifiers that are of type int or real expression.
     * @return true: if expression is valid, false: if expression is invalid
     * */
    bool numberExpressionVerify(string num1,string num2,string op);
    /*
     * validates number expression
     * @return true: if expression is valid, false: if expression is invalid
     * */
    bool numberCheck(string num1,string num2,string op);
    /*
     * verifies relation expression
     * lists all possible combinations
     * @return true: if expression is valid, false: if expression is invalid
     * */
    bool relationExpressionVerify(string num1,string num2,string op);

    bool check_relation_op(string op);//checks if operator is a relation op

    bool check_number_op(string op);//checks if operator is a number op

    bool check_bin_op(string op);//checks if operator is a binary op

    string getType(string num1);//checks the type for a given literal

    string error_message;//error message to be thrown

public:

    //initiates semantic analysis with a given symbol table and syntax tree;
    bool initiateSemantic(vector<AstStatementNode *> program,SymbolTable *st);

    //initiates semantic analysis with given functions mainly used for single line programs.
    bool initiateSemantic(vector<AstStatementNode *> program,SymbolTable *st,vector<func> functions);

    ~SemanticVisitor();;
    vector<func> getFunctions(); //stores functions
    SymbolTable *getST();//returns symbol table
    vector<AstFunctionNode *> getfunctionNodes();//returns function nodes.

};


#endif //CT_ASSIGNMENT_TEST_H
