

#include <cfloat>
#include "InterpreterVisitor.h"

void InterpreterVisitor::visit(AstBinaryNode *node) {
   if(node->RHS == nullptr && node->LHS == nullptr){
       if(ST->searchInScope(scope,node->operator_)){ // local check before global
           expression = ST->getScopeValue(scope,node->operator_);
       }else if(checkGlobal(node->operator_)){
           expression = getGlobalValue(node->operator_);
       }else
        expression = node->operator_;
       return;
   }
   node->LHS->accept(this);
   string num1 = expression;
   string op = node->operator_;
   string num2;
   if(node->RHS != nullptr){ // for unary operators
       node->RHS->accept(this);
       num2 = expression;


   }
    expressionIntrepretation(num1,num2,op);

}

void InterpreterVisitor::visit(AstFunctionNode *node) {
    if(isFuncCall){
        currentFuncName = node->identifier;
        for (paramcount = 0; paramcount < node->internal_parameters.size(); paramcount++) {
            node->internal_parameters[paramcount]->accept(this);
        }

        node->block->accept(this);
        isreturn = false;

    }else{
        funcNodes.push_back(node); //not evoked.
    }

}

InterpreterVisitor::~InterpreterVisitor() {
    delete ST;
    delete scope;
    for (int i = 0; i < funcNodes.size(); ++i) {
        delete funcNodes[i];
        funcNodes.pop_back();
    }

}

void InterpreterVisitor::visit(AstFunctionCallExpressionNode *node) {
    struct func *temp;
    for (auto &function : functions) {
        if(node->identifier == function.identifier){
            temp = &function;
        }
    }

    isFuncCall = true;
    subScopeCount+=1;
    for (auto &i : node->func_expr) {
        i->accept(this);
        temp->values.push_back(expression);
        expression.clear();
    }

    executeFunction(temp->identifier);
    isFuncCall = false;
    temp->values.clear();
}

void InterpreterVisitor::visit(AstIfStatementNode *node) {
    condition = true;
    node->ifcondition->accept(this);
    if(expression == "false")
        condition = false;

    if(condition){
        if(isFuncCall) //if not function call scoping is handled in the block statement
            subScopeCount+=1;
        node->if_statements->accept(this);
    }else if(node->else_statements != nullptr){
        if(isFuncCall)
            subScopeCount+=1;
        node->else_statements->accept(this);
    }
}

void InterpreterVisitor::visit(AstPrintStatementNode *node) {
    node->print->accept(this);
    cout<<expression<<endl; // print of expression
    curr_val = expression;
    expression.clear();
}

void InterpreterVisitor::visit(AstReturnStatementNode *node) {
    node->returnexp->accept(this);
    if(checkGlobalType(currentFuncName) == "int")
        floatToInt();
    curr_val = expression;
    isreturn = true;
}

void InterpreterVisitor::visit(AstUnaryExpresionNode *node) {

}

void InterpreterVisitor::visit(AstVarDeclareStatement *node) {
    if(subScopeCount >= 0){
        node->value->accept(this);
        if(ST->scopeTypeCheck(scope,node->identifier) == "int")
            floatToInt();
        ST->insert_in_scope(scope,0,subScopeCount,tuple<string,string,string>(node->identifier,typeCheck(node->type),expression));

    }else{
        node->value->accept(this);
        if(checkGlobalType(node->identifier) == "int")
            floatToInt();
        setGlobalValue(node->identifier);
    }
    curr_val = expression;
    expression.clear();
}

void InterpreterVisitor::visit(AstWhileStatementNode *node) {
    condition = true;
    int temp = 1;
    node->params->accept(this);
    if(expression == "false")
        temp = 0;
    expression.clear();
    while(temp == 1){
        if(isFuncCall)
            subScopeCount+=1;
        node->while_statements->accept(this);
        node->params->accept(this);
        if(expression == "false")
            temp = 0;
        expression.clear();
    }
}

void InterpreterVisitor::visit(ASTFormalParams *node) {
    ST->insert_in_scope(scope,0,subScopeCount,tuple<string,string,string>(node->identifier,typeCheck(node->type),parameterValueReturn()));
}


string InterpreterVisitor::typeCheck(lexer::Token_Type tokenType) {
    switch(tokenType){
        case TOK_INT:
            return "int";
        case TOK_REAL:
            return "real";
        case TOK_BOOL:
            return "bool";
        case TOK_STRLITERAL:
        case TOK_STRING:
            return "string";

        default:
            return "null";
    }
}
void InterpreterVisitor::visit(AstBlockStatementNode *node) {
    if(!isFuncCall)
        subScopeCount+=1;

    for (auto &b_statement : node->b_statements) {
        b_statement->accept(this);
        if(isreturn)
            break;
    }
    ST->remove_scope(scope,0,subScopeCount);
    subScopeCount-=1;
    if(subScopeCount == -1){
        scope = new Scope();
        ST->new_scope(scope);
    }
}

void InterpreterVisitor::visit(AstAssignmentStatementNode *node) {
    node->value->accept(this);
    if(subScopeCount >=0 && ST->searchInScope(scope,node->identifier)){
        if(ST->scopeTypeCheck(scope,node->identifier) == "int")
            floatToInt();
        ST->setValue(scope,node->identifier,expression);
    }else{
        if(checkGlobalType(node->identifier) == "int")
            floatToInt();
        setGlobalValue(node->identifier);
    }
    curr_val = expression;
    expression.clear();
}

void InterpreterVisitor::initiateInterpreter(SymbolTable *symbolTable, vector<AstStatementNode *> program,vector<func> functions) {
  this->ST = symbolTable;
  ST->new_scope(scope);
  this->functions = functions;
  for (auto &i : program) {
        i->accept(this);
  }

}
void InterpreterVisitor::initiateInterpreter(SymbolTable *symbolTable, vector<AstStatementNode *> program, vector<func> functions, vector<AstFunctionNode *> funcNodes) {
    this->ST = symbolTable;
    ST->new_scope(scope);
    this->functions = functions;
    this->funcNodes = funcNodes;
    for (auto &i : program) {
        i->accept(this);
    }
}
string InterpreterVisitor::parameterValueReturn() {
    for (auto &function : functions) {
        if(function.identifier == currentFuncName){
            return function.values[paramcount];
        }
    }
}

void InterpreterVisitor::executeFunction(string function) {
    for (auto &funcNode : funcNodes) {
        if(funcNode->identifier == function){
            funcNode->accept(this);
            break;
        }
    }
}

bool InterpreterVisitor::expressionIntrepretation(string num1, string num2, string op) {
    if(check_artimetic_op(op)){
        evaluateArtimeticExpression(num1,num2,op);
    }
    if(check_relation_op(op)){
        evaluateRelationExpression(num1,num2,op);
    }
    if(check_binary_op(op)){
        evaluateBinaryExpression(num1,num2,op);
    }

}
void InterpreterVisitor::evaluateArtimeticExpression(string num1, string num2, string op) {
    double num1_float = FLOATINIT;
    double num2_float = FLOATINIT;

    if(num1[0] == '-' && check_float(num1.substr(1,num1.length()-1)))
        num1_float = stof(num1); //unary only has left side
    else if(check_float(num1)){
        num1_float = stof(num1);

    }else if(ST->searchInScope(scope,num1)){
        num1_float = stof(ST->getScopeValue(scope,num1)) ;
    }else {
        num1_float = stof(valueGlobalCheck(num1));
    }
    if(num2.empty()){
        expression = to_string(-num1_float);
        return;
    }
    if(num2[0] == '-' && check_float(num2.substr(1,num2.length()-1)))
        num2_float = stof(num2);
    else if(check_float(num2)){
        num2_float = stof(num2);

    }else if(ST->searchInScope(scope,num2)){
        num2_float = stof(ST->getScopeValue(scope,num2)) ;
    }else if(checkGlobal(num2)){
        num2_float = stof(valueGlobalCheck(num2));
    }

    if(num2 == ""){
        expression = to_string(-num1_float);
    }




    if(op == "+")
        expression = to_string(num1_float+num2_float);
    else if(op == "/")
        expression = to_string(num1_float/num2_float);
    else if(op == "*")
        expression = to_string(num1_float*num2_float);
    else if(op == "-")
        expression = to_string(num1_float-num2_float);
}
void InterpreterVisitor::evaluateRelationExpression(string num1, string num2, string op) {
    /*NUMBER TO NUMBER RELATION*/
    auto num1_float = FLOATINIT;
    auto num2_float = FLOATINIT;

    if(check_float(num1)){
        num1_float = stof(num1);

    }else if(ST->searchInScope(scope,num1)){
        if(ST->scopeTypeCheck(scope,num1) == "int" ||ST->scopeTypeCheck(scope,num1) == "real")
             num1_float = stof(ST->getScopeValue(scope,num1));
    }else if(checkGlobal(num1)){
        if(checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real")
            num1_float = stof(valueGlobalCheck(num1));
    }



    if(check_float(num2)){
        num2_float = stof(num2);

    }else if(ST->searchInScope(scope,num2)){
        if(ST->scopeTypeCheck(scope,num2) == "int" ||ST->scopeTypeCheck(scope,num2) == "real")
            num2_float = stof(ST->getScopeValue(scope,num2));
    }else if(checkGlobal(num2)){
        if(checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real")
         num2_float = stof(valueGlobalCheck(num2));
    }

    if(num1_float != FLOATINIT && num2_float!= FLOATINIT){
        if(op == ">=")
            expression = relToBool(to_string(num1_float >= num2_float));
        else if(op == ">")
            expression = relToBool(to_string(num1_float > num2_float));
        else if(op == "<")
            expression = relToBool(to_string(num1_float < num2_float));
        else if(op == "<=")
            expression = relToBool(to_string(num1_float <= num2_float));
        else if(op == "!=")
            expression = relToBool(to_string(num1_float != num2_float));
        else if(op == "==")
            expression = relToBool(to_string(num1_float == num2_float));
        return;
    }

    /*STRING/BOOL TO STRING/BOOL RELATION*/
    if(ST->searchInScope(scope,num1)){
        num1 = ST->getScopeValue(scope,num1);

    }else if(checkGlobal(num1)){
        num1 = valueGlobalCheck(num1);
    }

    if(ST->searchInScope(scope,num2)){
        num2 = ST->getScopeValue(scope,num2);

    }else if(checkGlobal(num2)){
        num2 = valueGlobalCheck(num2);
    }

    if(!num2.empty() && !num2.empty()){
        if(op == "!=")
            expression = relToBool(to_string(num1 != num2));
        else if(op == "==")
            expression = relToBool(to_string(num1 == num2));
        return;
    }
}

bool InterpreterVisitor::check_digit(string number) {
    int i = 0;
    while(number[i] != '\0'){
        if(!isdigit(number[i]))
            return false;
        i++;
    }
    return true;
}

bool InterpreterVisitor::check_float(string number) {
    for (char i : number) {
        if (!isdigit(i) && i != '.')
            return false;
    }

    return true;
}
bool InterpreterVisitor::check_artimetic_op(string op) {
    return op == "*" || op == "/" || op == "+" || op == "-";
}

bool InterpreterVisitor::check_relation_op(string op) {
    return op == ">=" || op == ">" || op == "<=" || op == "<" || op == "!=" || op == "==";
}

bool InterpreterVisitor::check_binary_op(string op) {
    return false;
}

string InterpreterVisitor::valueGlobalCheck(string var) {
    for (auto &i : ST->global) {
        if(get<0>(i) == var)
            return get<2>(i);
    }
}

bool InterpreterVisitor::checkGlobal(string identifier) {
    for (auto &i : ST->global) {
        if(get<0>(i) == identifier)
            return true;
    }

    return false;
}

string InterpreterVisitor::checkGlobalType(string identifier) {
    for (auto &i : ST->global) {
        if(get<0>(i) == identifier)
            return get<1>(i);
    }
}

string InterpreterVisitor::relToBool(string res) {
    if(res == "0")
        return "false";
    else
        return "true";
}

void InterpreterVisitor::setGlobalValue(string identifier) {
    for (auto &i : ST->global) {
        if(get<0>(i) == identifier)
            get<2>(i) = expression;
    }
}

bool InterpreterVisitor::check_if_bool(string num) {
    return num == "true" || num == "false";
}

void InterpreterVisitor::evaluateBinaryExpression(string num1, string num2, string op) {
    if(ST->searchInScope(scope,num1)){
        num1 = ST->getScopeValue(scope,num1);

    }else if(checkGlobal(num1)){
        num1 = valueGlobalCheck(num1);
    }


    if(ST->searchInScope(scope,num2)){
        num2 = ST->getScopeValue(scope,num2);

    }else if(checkGlobal(num2)){
        num2 = valueGlobalCheck(num1);
    }

    if(!check_if_bool(num1)){
        num1 = "true";
    }

    if(!check_if_bool(num2)){
        num2 = "true";
    }

    if(op == "not"){
        if(num1 == "true")
            expression = "false";
        else
            expression = "true";
        return;
    }

    if(op == "and"){
        if(num1 == "false" || num2 == "false"){
            expression = "false";
        }else
            expression = "true";
        return;
    }

    if(op == "or"){
        if(num1 == "true" || num2 == "true"){
            expression = "true";
        }else
            expression = "false";
        return;
    }


}

void InterpreterVisitor::floatToInt() {
    string temp;
    for (char i : expression) {
        if(i == '.')
            break;
        temp += i;
    }
    expression = temp;
}

string InterpreterVisitor::getGlobalValue(string identifier) {
    for (auto &i : ST->global) {
        if(get<0>(i) == identifier)
            return get<2>(i);
    }
}

void InterpreterVisitor::visit(AstFunctionCallStatement *node) {
    node->functionCall->accept(this);
}






