#ifndef CT_ASSIGNMENT_VISITOR_H
#define CT_ASSIGNMENT_VISITOR_H

class AstFunctionNode;
class AstFunctionNode;
class AstFunctionCallExpressionNode;
class AstIfStatementNode;
class AstPrintStatementNode;
class AstReturnStatementNode;
class AstUnaryExpresionNode;
class AstVarDeclareStatement;
class AstWhileStatementNode;
class AstBinaryNode;
class AstExpressionNode;
class ASTFormalParams;
class AstBlockStatementNode;
class AstAssignmentStatementNode;
class AstStatementNode;
class AstFunctionCallStatement;

class Visitor {
public:
    virtual void visit(AstBinaryNode *node) = 0;
    virtual void visit(AstFunctionNode *node) = 0;
    virtual void visit(AstFunctionCallExpressionNode *node) = 0;
    virtual void visit(AstIfStatementNode *node) = 0;
    virtual void visit(AstPrintStatementNode *node) = 0;
    virtual void visit(AstReturnStatementNode *node) = 0;
    virtual void visit(AstUnaryExpresionNode *node) = 0;
    virtual void visit(AstVarDeclareStatement *node) = 0;
    virtual void visit(AstWhileStatementNode *node) = 0;
    virtual void visit(ASTFormalParams *node) = 0;
    virtual void visit(AstBlockStatementNode *node) = 0;
    virtual void visit(AstAssignmentStatementNode *node) = 0;
    virtual void visit(AstFunctionCallStatement *node) = 0;

};


#endif //CT_ASSIGNMENT_VISITOR_H
