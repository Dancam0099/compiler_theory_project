#include <sstream>
#include "semanticVisitor.h"
#include "../parser/ast_funcDec.h"
#include "../parser/ast_while_statement.h"
#include "../parser/ast_var_declare_statement.h"
#include "../parser/ast_assignment_statement.h"
#include "../parser/ast_if_statement.h"
#include "../parser/ast_function_call_expression_node.h"
#include "../parser/ast_binary_expression_node.h"
#include "../parser/ast_print_statement.h"
#include "../parser/ast_return_statement.h"
#include "../parser/Ast_Unary_Expression_node.h"
#include "InterpreterVisitor.h"
#include "../Exceptions/SemanticExceptions.h"

void SemanticVisitor::visit(AstFunctionNode *node) {
    if(sub_scopeC > 0 && ST->scopeIn != nullptr){
        throw SemanticExceptions("functions within functions are not allowed");
    }

    struct func func1;
    if(!checkGlobal(node->identifier) && typeCheck(node->type) != "null"){
        func1.identifier = node->identifier;
        func1.type = typeCheck(node->type);
        func1.returnc = 0;
    }else throw SemanticExceptions("function already exists");
    functions.push_back(func1);
    for (auto &internal_parameter : node->internal_parameters) {
        internal_parameter->accept(this);
    }

    isfunc = true;
    node->block->accept(this);
    isfunc = false;
    if(functions[func_count].returnc < 1){
        functions.pop_back();

        throw SemanticExceptions("error no return in function");
    }
    func_count+=1;
    ST->global.emplace_back(node->identifier,typeCheck(node->type),"function");
    funcNodes.push_back(node);
}


string SemanticVisitor::typeCheck(lexer::Token_Type tokenType) {
    switch(tokenType){
        case TOK_INT:
            return "int";
        case TOK_REAL:
            return "real";
        case TOK_BOOL:
            return "bool";
        case TOK_STRLITERAL:
        case TOK_STRING:
            return "string";

        default:
            return "null";
    }
}

void SemanticVisitor::visit(ASTFormalParams *node) {
    if(typeCheck(node->type) != "null" && !ST->searchInScope(scope,node->identifier)){
        ST->scopeIn->variables.emplace_back(node->identifier,typeCheck(node->type),"");
        functions[func_count].param_types.push_back(typeCheck(node->type));
    }else{
        throw SemanticExceptions("Same Parameter decleration");

    }
}


void SemanticVisitor::visit(AstBlockStatementNode *node) {

    sub_scopeC+=1;
    for (auto &b_statement : node->b_statements) {
        b_statement->accept(this);
    }
    if(scope!= nullptr){
        ST->remove_scope(scope,0,sub_scopeC);
    }
    sub_scopeC-=1;
    if(sub_scopeC == -1){
        scope = new Scope();
        ST->new_scope(scope);
    }

}

bool SemanticVisitor::initiateSemantic(vector<AstStatementNode *> program, SymbolTable *st) {
    ST = st;
    ST->new_scope(scope);
    for (auto &i : program) {
        try{
            i->accept(this);
        }catch (SemanticExceptions){
            return false;
        }
    }
    return true;
}

bool SemanticVisitor::initiateSemantic(vector<AstStatementNode *> program, SymbolTable *st, vector<func> functions) {
    ST = st;
    ST->new_scope(scope);
    this->functions = functions;
    for (auto &i : program) {
        try{
            i->accept(this);
        }catch (SemanticExceptions){
            if(isfunc)
                functions.pop_back();
            return false;
        }
    }
    return true;
}
void SemanticVisitor::visit(AstAssignmentStatementNode *node) {
    if(sub_scopeC >= 0){
        if(ST->searchInScope(scope,node->identifier)){
            varExp = true;
            currType = ST->scopeTypeCheck(scope,node->identifier);
            node->value->accept(this);
            varExp = false;
            temp_expr = "";
            return;
        }

    }

    if(checkGlobal(node->identifier)){
            varExp = true;
            currType = checkGlobalType(node->identifier);
            node->value->accept(this);
            varExp = false;
            temp_expr = "";
            return;
    }else{
        throw SemanticExceptions(node->identifier+" Variable Does Not Exist");
    }

}

void SemanticVisitor::visit(AstWhileStatementNode *node) {

        node->params->accept(this);
        temp_expr = "";
        node->while_statements->accept(this);

}

void SemanticVisitor::visit(AstVarDeclareStatement *node) {

    if(sub_scopeC >=0){
        if(typeCheck(node->type) != "null" && !ST->searchInScope(scope,node->identifier)){
            varExp = true;
            currType = typeCheck(node->type);
            node->value->accept(this);
            varExp = false;
            ST->insert_in_scope(scope,0,sub_scopeC,tuple<string,string,string>(node->identifier,typeCheck(node->type),""));
            temp_expr = "";
            return;
        }else if(ST->searchInScope(scope,node->identifier)){
            throw SemanticExceptions(node->identifier+" Variable Already Exists");
        }
    }

    if(typeCheck(node->type) != "null" && !checkGlobal(node->identifier)){
            varExp = true;
            currType = typeCheck(node->type);
            node->value->accept(this);
            varExp = false;
            ST->global.emplace_back(tuple<string,string,string>(node->identifier,typeCheck(node->type),""));
            temp_expr = "";
            return;
    }else{
        throw SemanticExceptions(node->identifier+" Variable Already Exists");
    }

}

void SemanticVisitor::visit(AstUnaryExpresionNode *node) {
    node->un_exp->accept(this);
    temp_expr = node->Unary_op;
}

void SemanticVisitor::visit(AstReturnStatementNode *node) {
    if(isfunc){
        currType = functions[func_count].type;
        varExp = true;
        node->returnexp->accept(this);
        functions[func_count].returnc+=1;
        varExp = false;
        temp_expr = "";

    }else{
        throw SemanticExceptions("Return statement not allowed outside a function");
    }


}

void SemanticVisitor::visit(AstPrintStatementNode *node) {

    varExp = true;
    currType = "print";
    node->print->accept(this);
    varExp = false;
    temp_expr = "";
}

void SemanticVisitor::visit(AstIfStatementNode *node) {
    node->ifcondition->accept(this);
    temp_expr = "";

    node->if_statements->accept(this);


    if (node->else_statements) {
        node->else_statements->accept(this);
    }
}

void SemanticVisitor::visit(AstFunctionCallExpressionNode *node) {
 if(checkGlobal(node->identifier)){
     if(node->identifier == functions[func_count].identifier)
         throw SemanticExceptions("Recursion is not allowed");
     if(funCall)
         throw SemanticExceptions("Function calls within function calls are not allowed");
     struct func temp;
     for (auto &function : functions) {
         if(node->identifier == function.identifier){
             temp = function;
         }
     }

     if(temp.param_types.size() == node->func_expr.size()){
         funCall = true;
         for (int i = 0; i < node->func_expr.size(); ++i) {
             currFuncParType = temp.param_types[i];
             node->func_expr[i]->accept(this);
         }
         funCall = false;

         if(temp.type == "int" || temp.type == "real"){
             temp_expr = "0";
         }else if(temp.type == "string"){
             temp_expr = "\"\"";
         }else if(temp.type == "bool"){
             temp_expr = "true";
         }

     }else{
         throw SemanticExceptions("Incorrect amount of parameters");
     }



 }else{
     throw SemanticExceptions(node->identifier+" function does not exist");
 }
}

void SemanticVisitor::visit(AstBinaryNode *node) {

    if(node->LHS == nullptr && node->RHS == nullptr ){
        if(funCall){
            if(!varTypeCheck(node->operator_)){
                throw SemanticExceptions("ERROR: "+node->operator_+" function call type does not match");
            }
        }else if(varExp){
            if(!varTypeCheck(node->operator_)){
                throw SemanticExceptions("ERROR: type mismatch in var,set or print");
            }
        }
        temp_expr.append(node->operator_);
        return;
    }

    node->LHS->accept(this);
    string num1 =temp_expr;
    string op = node->operator_;
    temp_expr = "";
    string num2;
    if(node->RHS != nullptr){
        node->RHS->accept(this);
        num2 = temp_expr;
        temp_expr = "";

    }
    if(!expressionVerify(num1,num2,op)){
        throw SemanticExceptions(error_message);
    };


}

bool SemanticVisitor::checkGlobal(string identifier) {

    for (auto &i : ST->global) {
        if(get<0>(i) == identifier)
            return true;
    }

    return false;
}



bool SemanticVisitor::check_digit(string number) {
    int i = 0;
    while(number[i] != '\0'){
        if(!isdigit(number[i]))
            return false;
        i++;
    }
    return true;
}

bool SemanticVisitor::check_float(string number) {
    for (char i : number) {
        if (!isdigit(i) && i != '.')
            return false;
    }

    return true;
}

string SemanticVisitor::checkGlobalType(string identifier) {
    for (auto &i : ST->global) {
        if(get<0>(i) == identifier)
            return get<1>(i);
    }


}

bool SemanticVisitor::expressionVerify(string num1,string num2,string op) {
    if(check_number_op(op) && numberExpressionVerify(num1,num2,op)){
        return true;
    }else if(check_relation_op(op)&& !varExp && relationExpressionVerify(num1,num2,op)) {
        return true;
    }else if(check_relation_op(op) && varExp){
        error_message = "ERROR cannot use relationship operations in var or set";
        return false;
    }

    return false;


}

bool SemanticVisitor::check_relation_op(string op) {
    return op == ">=" || op == ">" || op == "<=" || op == "<" || op == "!=" || op == "==";
}

bool SemanticVisitor::check_number_op(string op) {
    return op == "*" || op == "/" || op == "+" || op == "-";
}

bool SemanticVisitor::numberExpressionVerify(string num1, string num2, string op) {
    if(numberCheck(num1,num2,op))
        return true;
    if(ST->searchInScope(scope,num1) && ST->searchInScope(scope,num2)){
        if((ST->scopeTypeCheck(scope,num1) == "int" || ST->scopeTypeCheck(scope,num1) == "real") && ((ST->scopeTypeCheck(scope,num2) == "int" || ST->scopeTypeCheck(scope,num2) == "real")) ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    } else if(checkGlobal(num1) && ST->searchInScope(scope,num2)){
        if((checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real") && (ST->scopeTypeCheck(scope,num2) == "int" || ST->scopeTypeCheck(scope,num2) == "real") ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }else if(ST->searchInScope(scope,num1) && checkGlobal(num2)){
        if((checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real") && (ST->scopeTypeCheck(scope,num1) == "int" || ST->scopeTypeCheck(scope,num1) == "real") ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if(checkGlobal(num1) && checkGlobal(num2)){
        if((checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real") && (checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real") ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }
    error_message = "ERROR: cannot match: "+num1+" with: "+num2+" using operation: "+op;
    return false;
}

bool SemanticVisitor::relationExpressionVerify(string num1, string num2, string op) {
    //binary operators
    if(!num1.empty() && check_bin_op(op) && !num2.empty()){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }

    //two operators
    if(check_number_op(num1) && check_number_op(num2)){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }
    //two rel ops
    if(check_relation_op(num1) && check_relation_op(num2) &&(op == "!=" || op == "==") ){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }
    // one rel op and bool
    if((op == "!=" || op == "==") && ((check_relation_op(num1) && (num2 == "true" || num2 == "false")) || (check_relation_op(num2) && (num1 == "true" || num1 == "false")))){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }

    // one rel op and string
    if(check_relation_op(num1) && (num2[0] == '"') || check_relation_op(num2) && (num1[0] == '"') &&(op == "!=" || op == "==") ){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }
    //numbers and rel op
    if(check_relation_op(num1) && (check_digit(num2) || check_float(num2)) &&(op == "!=" || op == "==") ){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }

    //numbers and rel op
    if(check_relation_op(num2) && (check_digit(num1) || check_float(num1)) &&(op == "!=" || op == "==") ){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }
    //rel op and variables
    if((check_relation_op(num2)) && ST->searchInScope(scope,num1) &&(op == "!=" || op == "==") ){
        if(ST->scopeTypeCheck(scope,num1) == "int" || ST->scopeTypeCheck(scope,num1) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(ST->scopeTypeCheck(scope,num1) == "bool" && (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if((check_number_op(num2)) && checkGlobal(num1) &&(op == "!=" || op == "==") ){
        if(checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num1) == "bool" && (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }
    //rel op and variables
    if((check_relation_op(num1)) && ST->searchInScope(scope,num2) &&(op == "!=" || op == "==") ){
        if(ST->scopeTypeCheck(scope,num2) == "int" || ST->scopeTypeCheck(scope,num2) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(ST->scopeTypeCheck(scope,num2) == "bool" && (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if((check_number_op(num1)) && checkGlobal(num2) &&(op == "!=" || op == "==") ){
        if(checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num2) == "bool" && (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }

    //operators and numbers
    if(check_number_op(num1) && (check_digit(num2) || check_float(num2))){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }

    //operators and numbers
    if(check_number_op(num2) && (check_digit(num1) || check_float(num1))){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }

    //operators and variables
    if((check_number_op(num1)) && ST->searchInScope(scope,num2)){
        if(ST->scopeTypeCheck(scope,num2) == "int" || ST->scopeTypeCheck(scope,num2) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if((check_number_op(num1)) && checkGlobal(num2)){
        if(checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }

    //operators and variables
    if((check_number_op(num2)) && ST->searchInScope(scope,num1)){
        if(ST->scopeTypeCheck(scope,num1) == "int" || ST->scopeTypeCheck(scope,num1) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if((check_number_op(num2)) && checkGlobal(num1)){
        if(checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }

    if(numberCheck(num1,num2,op))
        return true;

    //two variables
    if(ST->searchInScope(scope,num1) && ST->searchInScope(scope,num2)){
        if((ST->scopeTypeCheck(scope,num1) == "int" || ST->scopeTypeCheck(scope,num1) == "real") && ((ST->scopeTypeCheck(scope,num2) == "int" || ST->scopeTypeCheck(scope,num2) == "real")) ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(ST->scopeTypeCheck(scope,num1) == "bool" && ST->scopeTypeCheck(scope,num2) == "bool"&& (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(ST->scopeTypeCheck(scope,num1) == "string" && ST->scopeTypeCheck(scope,num2) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if(checkGlobal(num1) && ST->searchInScope(scope,num2)){
        if((checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real") && (ST->scopeTypeCheck(scope,num2) == "int" || ST->scopeTypeCheck(scope,num2) == "real") ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num1) == "bool" && ST->scopeTypeCheck(scope,num2) == "bool"&& (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num1) == "string" && ST->scopeTypeCheck(scope,num2) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }else if(ST->searchInScope(scope,num1) && checkGlobal(num2)){
        if((checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real") && (ST->scopeTypeCheck(scope,num1) == "int" || ST->scopeTypeCheck(scope,num1) == "real") ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num2) == "bool" && ST->scopeTypeCheck(scope,num2) == "bool"&& (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num2) == "string" && ST->scopeTypeCheck(scope,num2) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if(checkGlobal(num1) && checkGlobal(num2)){
        if((checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real") && (checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real") ){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num1) == "bool" && checkGlobalType(num2) == "bool" && (op == "!=" || op == "==")){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }else if(checkGlobalType(num1) == "string" && checkGlobalType(num2) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }

    //bool and variable
    if((num1 == "false" || num1 == "true") && ST->searchInScope(scope,num2) && (op == "!=" || op == "==")){
        if(ST->scopeTypeCheck(scope,num2) == "bool"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }else if((num1 == "false" || num1 == "true") && checkGlobal(num2)){
        if(checkGlobalType(num2) == "bool"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }

    //bool and variable
    if((num2 == "false" || num2 == "true") && ST->searchInScope(scope,num1) && (op == "!=" || op == "==")){
        if(ST->scopeTypeCheck(scope,num1) == "bool"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }else if((num2 == "false" || num2 == "true") && checkGlobal(num1)){
        if(checkGlobalType(num1) == "bool"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }

    //string and var
    if(num1[0] == '"' && ST->searchInScope(scope,num2)){
        if(ST->scopeTypeCheck(scope,num2) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }else if(num1[0] == '"' && checkGlobal(num2)){
        if(checkGlobalType(num2) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }

    //string and var
    if(num2[0] == '"' && ST->searchInScope(scope,num1)){
        if(ST->scopeTypeCheck(scope,num1) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }
    }else if(num2[0] == '"' && checkGlobal(num1) ){
        if(checkGlobalType(num1) == "string"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }
    error_message = "ERROR: cannot match: "+num1+" with: "+num2+" using operation: "+op;
    return false;

}

bool SemanticVisitor::numberCheck(string num1, string num2,string op) {
    if((check_digit(num1) || check_float(num1)) && op == "-" && num2.empty())
        return true;
    if((check_digit(num1) || check_float(num1)) && (check_digit(num2) || check_float(num2))){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }

    if((check_digit(num1) || check_float(num1)) && check_number_op(num2)){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }

    if((check_digit(num2) || check_float(num2)) && check_number_op(num1)){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }


    if(check_number_op(num2) && check_number_op(num1)){
        temp_expr.clear();
        temp_expr.append(op);
        return true;
    }
    if((check_digit(num1) || check_float(num1)) && ST->searchInScope(scope,num2)){
        if(ST->scopeTypeCheck(scope,num2) == "int" || ST->scopeTypeCheck(scope,num2) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if((check_digit(num1) || check_float(num1)) && checkGlobal(num2)){
        if(checkGlobalType(num2) == "int" || checkGlobalType(num2) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }

    if((check_digit(num2) || check_float(num2)) && ST->searchInScope(scope,num1)){
        if(ST->scopeTypeCheck(scope,num1) == "int" || ST->scopeTypeCheck(scope,num1) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }else if((check_digit(num2) || check_float(num2)) && checkGlobal(num1)){
        if(checkGlobalType(num1) == "int" || checkGlobalType(num1) == "real"){
            temp_expr.clear();
            temp_expr.append(op);
            return true;
        }

    }
    return false;
}

bool SemanticVisitor::check_bin_op(string op) {
    return op == "and" || op == "or" || op == "not";


}

bool SemanticVisitor::varTypeCheck(string num1) {
    string type;

    if(funCall){
        type = currFuncParType;

    }else{
        type = currType;

    }

    if(type == "print" && getType(num1)!="null"){
        type = getType(num1);
    }

    if((type == "int" || type == "real")&&(getType(num1) == "int" || getType(num1) == "real"))
        return true;
    else return type == getType(num1);

}

vector<func> SemanticVisitor::getFunctions() {
    return functions;
}

string SemanticVisitor::getType(string num1) {
    if(check_digit(num1))
        return "int";
    else if(check_float(num1))
        return "real";
    else if(num1 == "true" || num1 == "false")
        return "bool";
    else if(num1[0] == '\"')
        return "string";
    else if(ST->searchInScope(scope,num1))
        return ST->scopeTypeCheck(scope,num1);
    else if(checkGlobal(num1))
        return checkGlobalType(num1);
    else
        return "null";
}

SymbolTable *SemanticVisitor::getST() {
    return ST;

}

vector<AstFunctionNode *> SemanticVisitor::getfunctionNodes() {
    return funcNodes;
}

void SemanticVisitor::visit(AstFunctionCallStatement *node) {
    node->functionCall->accept(this);
}

SemanticVisitor::~SemanticVisitor() {
    delete  ST;
    delete scope;
    for (int i = 0; i < funcNodes.size(); ++i) {
        delete funcNodes[i];
        funcNodes.pop_back();
    }
}





