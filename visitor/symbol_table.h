#ifndef CT_ASSIGNMENT_SYMBOL_TABLE_H
#define CT_ASSIGNMENT_SYMBOL_TABLE_H

#include <tuple>
#include "../Includer.h"
#include "../lexer/token.h"

struct func{
    string identifier;
    string type;
    vector<string> param_types;
    vector<string> values;
    int returnc;
};

class Scope{
public:
    vector<tuple<string,string,string>> variables;
    Scope *scope;
    ~Scope();

};

class SymbolTable{
    string typeCheck(lexer::Token_Type tokenType);
public:

    vector<tuple<string,string,string>> global; // global variables and functions
    Scope * scopeIn; // scopes

    void new_scope(Scope *scope); // links scope with semantic and interpreter scope

    //Inserts a variable at a given scope
    void insert_in_scope(Scope *scp,int start,int stop,tuple<string,string,string> variable);
    void createScope(Scope *scp,int start,int stop);
    //removes linked scope
    void remove_scope(Scope *scp,int start,int stop);
    //searches for variable within scope
    bool searchInScope(Scope * scope,string var);
    //sets the value for a variable within a scope
    void setValue(Scope *scope,string var,string value);
    //returns the type for variable that already exists in the scope
    string scopeTypeCheck(Scope * scope,string var);

    //returns the value of a variable within scope
    string getScopeValue(Scope *scope,string var);
    ~SymbolTable();

};


#endif //CT_ASSIGNMENT_SYMBOL_TABLE_H
