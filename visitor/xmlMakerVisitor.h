//
// Created by daniel on 4/17/18.
//

#ifndef CT_ASSIGNMENT_XMLMAKERVISITOR_H
#define CT_ASSIGNMENT_XMLMAKERVISITOR_H
#include "../Includer.h"
#include "visitor.h"
#include "../lexer/lexer.h"

#include "visitor.h"
#include "../parser/ast_function_call_expression_node.h"

class XmlMakerVisitor : public Visitor{

    ofstream xmlfile; // file stream
    int indent; // indentation quantity
public:
    /*
     * creates xml file in the directory compiler_theory_project/cmake-build-debug
     * */
    XmlMakerVisitor();
    /*
     * creates proper tags for binary nodes
     * Tags :  <BinExprNode Op = Operator></BinExprNode>,
     *         <real>value</real>
     *         <int>value</int>
     *         <bool>value</bool>
     *         <string>value</string>
     * function call tags are also stored within <BinExprNode>
     * */
    void visit(AstBinaryNode *node) override;
    /*
     * creates proper tags for function definition
     * Tags : <Func></Func>
     *        <Function Type = Operator>identifier</Function>
     *        <Function_Params>function parameters</Function_Params>
	 *	      <Function_Statements>block statement</Function_Statements>
     * */
    void visit(AstFunctionNode *node) override;
    /*
     * creates proper tags for function calls
     * Tags : <Func_call></Func_call>
     *        <Function_Id>identifier</Function_Id>
     *        <Actual_Params></Actual_Params>
     * can be stored within an expression
     * */
    void visit(AstFunctionCallExpressionNode *node) override;
    /*
     * creates proper tags for if condition
     * Tags : <If></If>
     *        <If_Condition>expression</If_Condition>
     *        <If_Statements>block statement</If_Statements>
     *        <Else_Statements>block statement</Else_Statements>
     * */
    void visit(AstIfStatementNode *node) override;
    /*
     * creates proper tags for printing an expression
     * Tags : <Print>expression</Print>
     * */
    void visit(AstPrintStatementNode *node) override;
    /*
     * creates proper tags for return statement
     * Tags : <Return>expression</Return>
     * */
    void visit(AstReturnStatementNode *node) override;
    /*
     * creates proper tags for unary Expression
     * Tags : <Unary>expression</Unary>
     * */
    void visit(AstUnaryExpresionNode *node) override;
    /*
     * creates proper tags for variable decleration
     * Tags : <Decl></Decl>
     *        <Id Type =Operator>identifier</Id>
     * */
    void visit(AstVarDeclareStatement *node) override;
    /*
     * creates proper tags for while statement node
     * Tags : <While></While>
     *        <While_Param>epxression</While_Param>
     *        <While_Statements>block statement</While_Statements>
     * */
    void visit(AstWhileStatementNode *node) override;
    /*
     * creates proper tags for formal paramters
     * Tags : <Id Type = Operator>identifier</Id>
     * */
    void visit(ASTFormalParams *node) override;
    /*
     * accepts each statement in the block
     * */
    void visit(AstBlockStatementNode *node) override;
    /*
     * creates proper tags for assignment statement
     * Tags : <Set></Set>
     *        <Id>identifier</Id>
     * */
    void visit(AstAssignmentStatementNode *node) override;
    /*
     * calls function call for given function
     * */
    void visit(AstFunctionCallStatement *node) override;
    /*
     * Initiates XML creation and uses proper tags for program
     * Tags: <Program></Program>
     * */
    void InitiateXML(vector<AstStatementNode *> syntaxtree);

    string typeCheck(lexer::Token_Type tokenType); // returns the type for a token

};


#endif //CT_ASSIGNMENT_XMLMAKERVISITOR_H
