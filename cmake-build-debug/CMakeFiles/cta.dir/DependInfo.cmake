# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/daniel/compiler_theory_project/lexer/lexer.cpp" "/home/daniel/compiler_theory_project/cmake-build-debug/CMakeFiles/cta.dir/lexer/lexer.cpp.o"
  "/home/daniel/compiler_theory_project/lexer/states.cpp" "/home/daniel/compiler_theory_project/cmake-build-debug/CMakeFiles/cta.dir/lexer/states.cpp.o"
  "/home/daniel/compiler_theory_project/lexer/token.cpp" "/home/daniel/compiler_theory_project/cmake-build-debug/CMakeFiles/cta.dir/lexer/token.cpp.o"
  "/home/daniel/compiler_theory_project/main.cpp" "/home/daniel/compiler_theory_project/cmake-build-debug/CMakeFiles/cta.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
