//
// Created by daniel on 3/28/18.
//

#ifndef CT_ASSIGNMENT_AST_FORMALPARAMS_H
#define CT_ASSIGNMENT_AST_FORMALPARAMS_H

#include "../Includer.h"
#include "../lexer/token.h"
#include "../visitor/visitor.h"

using namespace lexer;
class ASTFormalParams {
public:
    void accept(Visitor *node); // visitor
    string identifier; // identifier
    Token_Type type; // type of identifier
};


#endif //CT_ASSIGNMENT_AST_FORMALPARAMS_H
