#ifndef CT_ASSIGNMENT_AST_RETURN_STATEMENT_H
#define CT_ASSIGNMENT_AST_RETURN_STATEMENT_H


#include "ast_statement_node.h"
#include "ast_expression_node.h"

class AstReturnStatementNode : public AstStatementNode{
public:
    AstExpressionNode *returnexp; //return expression
    ~AstReturnStatementNode();
    void accept(Visitor *visitor);

};


#endif //CT_ASSIGNMENT_AST_RETURN_STATEMENT_H
