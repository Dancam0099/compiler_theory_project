//
// Created by daniel on 4/2/18.
//

#ifndef CT_ASSIGNMENT_AST_PRINT_STATEMENT_H
#define CT_ASSIGNMENT_AST_PRINT_STATEMENT_H


#include "ast_statement_node.h"
#include "ast_expression_node.h"

class AstPrintStatementNode: public AstStatementNode {
public:

    AstExpressionNode *print; // expression to print
    ~AstPrintStatementNode();
    void accept(Visitor *visitor);
};


#endif //CT_ASSIGNMENT_AST_PRINT_STATEMENT_H
