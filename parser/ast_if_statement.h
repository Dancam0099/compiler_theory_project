#ifndef CT_ASSIGNMENT_AST_IF_STATEMENT_H
#define CT_ASSIGNMENT_AST_IF_STATEMENT_H


#include "ast_statement_node.h"
#include "ast_expression_node.h"
#include "ast_block_statement.h"

class PrintInfoVisitor;
class AstIfStatementNode : public AstStatementNode {
public:
     AstExpressionNode *ifcondition; // condition expression
     AstBlockStatementNode *if_statements; // statements
     AstBlockStatementNode *else_statements; // statements
     ~AstIfStatementNode(); //destructor
    void accept(Visitor *visitor); //visitor
};


#endif //CT_ASSIGNMENT_AST_IF_STATEMENT_H
