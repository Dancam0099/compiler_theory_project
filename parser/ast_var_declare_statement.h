#ifndef CT_ASSIGNMENT_AST_VAR_DECLARE_STATEMENT_H
#define CT_ASSIGNMENT_AST_VAR_DECLARE_STATEMENT_H
#include "ast_statement_node.h"
#include "ast_expression_node.h"

using namespace lexer;

class AstVarDeclareStatement : public AstStatementNode{
public:
    string identifier;
    Token_Type type;
    AstExpressionNode *value; // value of deceleration
    ~AstVarDeclareStatement();
    void accept(Visitor *visitor);
};


#endif //CT_ASSIGNMENT_AST_VAR_DECLARE_STATEMENT_H
