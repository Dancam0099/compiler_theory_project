#include "ast_if_statement.h"

AstIfStatementNode::~AstIfStatementNode() {
    delete ifcondition;
    delete if_statements;
    delete else_statements;
}

void AstIfStatementNode::accept(Visitor *visitor) {
    visitor->visit(this);
}
