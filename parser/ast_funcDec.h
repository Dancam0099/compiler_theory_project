#ifndef CT_ASSIGNMENT_AST_FUNCDEC_H
#define CT_ASSIGNMENT_AST_FUNCDEC_H
#include "ast_statement_node.h"
#include "ast_formalparams.h"
#include "ast_block_statement.h"

class AstFunctionNode : public AstStatementNode {
public:
    string identifier;
    vector<ASTFormalParams *> internal_parameters; // function parameters
    lexer::Token_Type type; // type of function
    AstBlockStatementNode *block; // function statements
    ~AstFunctionNode(); // destructor
    void accept(Visitor *visitor); //visitor
};


#endif //CT_ASSIGNMENT_AST_FUNCDEC_H
