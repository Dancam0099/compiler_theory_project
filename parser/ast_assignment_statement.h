//
// Created by daniel on 3/28/18.
//

#ifndef CT_ASSIGNMENT_AST_ASSIGNMENT_STATEMENT_H
#define CT_ASSIGNMENT_AST_ASSIGNMENT_STATEMENT_H

#include "ast_statement_node.h"
#include "ast_expression_node.h"
#include "../lexer/token.h"
#include "../visitor/visitor.h"


class AstAssignmentStatementNode : public AstStatementNode{
public:
    string identifier; // variable identifier
    AstExpressionNode *value; // value of variable
    void accept(Visitor *visitor); // visitor
    ~AstAssignmentStatementNode(); //destructor


};


#endif //CT_ASSIGNMENT_AST_ASSIGNMENT_STATEMENT_H
