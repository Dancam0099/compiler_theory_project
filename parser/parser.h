#ifndef CT_ASSIGNMENT_PARSER_H
#define CT_ASSIGNMENT_PARSER_H

#include "astnode.h"
#include "../lexer/lexer.h"
#include "ast_funcDec.h"
#include "ast_formalparams.h"
#include "ast_var_declare_statement.h"
#include "../visitor/visitor.h"
#include "../visitor/xmlMakerVisitor.h"
#include "../visitor/semanticVisitor.h"


namespace parser{
    class Parser {

        lexer::Lexer *lex; // lexer object
        vector<AstStatementNode *> statements; //syntax tree
        XmlMakerVisitor *xmlMaker; // xml creator object

        /*
         * Parses a statement node by getting the first token of each line.
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @return returns a statement node that was successfully parsed.
         * */
        AstStatementNode *ParseStatement();

        /*
         * Parses a function deceleration statement by getting tokens using the lex object
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @returns returns a function deceleration statement node when successfully parsed
         * */
        AstFunctionNode *ParseFunctionDecleration();

        /*
         * Parses paramaters of function deceleration.
         *
         * @param params - stores parameters.
         * @throws ParserExceptions - whenever an unexpected token is found
         * @return - function parameters parsed and stored in vector.
         * */
        vector<ASTFormalParams *> ParseFormalParams(vector<ASTFormalParams *> params);

        /*
         * Parses variable deceleration.
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @return - returns a variable deceleration object when successfully parsed
         * */
        AstVarDeclareStatement *ParseVarDeclare();

        /*
         * Parses a block statement by parsing every statement
         * after an open square bracket and stops until a closed square bracket is found
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @returns - an object which contains a vector of parsed statements.
         * */
        AstBlockStatementNode *ParseBlockStatement();

        /*
         * Parses and if statement by first parsing the expression and afterwards a block statement is parsed.
         * else is only parsed when the token after square bracket is of type else
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @returns - object of type AstStatementNode when successfully parsed
         * */
        AstStatementNode *ParseIfStatement();

        /*
         * Parses while expression afterwards the statements of the while loop are parsed by a
         * block statement.
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @returns - object which will contain an expression and a block
         * */
        AstStatementNode *ParseWhileStatement();

        /*
         * parses print expression.
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @returns - object which will contain an expression.
         * */
        AstStatementNode *ParsePrintStatement();

        /*
         * Parses variable assignment and the expression used.
         *
         * @throws ParserExceptions - whenever an unexpected token is found
         * @returns - object which will contain an expression and variable to assign.
         * */
        AstStatementNode *ParseAssignmentStatement();

        /*
         * Parses expression of the return statement.
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - object which will contain an expression.
         * */
        AstStatementNode *ParseReturnStatement();

        /*
         * Parses function call expression.
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - object with function call expression object.
         * */
        AstStatementNode *ParseFunctioncall();

        /*
         * Parses exression and after checks if the operator is a relation operator.
         * After the RHS is parsed.
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - expression object.
         * */
        AstExpressionNode *ParseExpression();

        /*
         * Parses simple expression and after checks if the operator is an arthimetic operator or or operator.
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - simple expression object.
         * */
        AstExpressionNode *ParseSimpleExpression();

        /*
         * Parses term node and after checks if the operator is an multiplication operator or and operator.
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - term expression object.
         * */
        AstExpressionNode *ParseTerm();

        /*
         * Parses a factor which can either be a function call,literal,identifier, not operator or negative operator.
         *
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - factor object after parsing.
         * */
        AstExpressionNode *ParseFactor();

        /*
         * Parses an expression within brackets
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns -expression object.
         * */
        AstExpressionNode *ParseSubExpression();

        /*
         * Parses function call and the parameters within the function call.
         * Matches the amount of commas with the amount of function parameters.
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns -function call expression object.
         * */
        AstExpressionNode *ParseFunctionCall(string identifier);

        /*
         * Parses not unary call by using ParseFactor()
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - unary expression object.
         * */
        AstExpressionNode *ParseUnaryCall();



        /*
         * checks the type for the identifier to ensure thats its either bool,int,real or string.
         *
         * @param type - the token type of the current lexeme
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - true if type is relevant to var or set || false if type is not relevant to var or set.
         * */
        bool checkIdentifierType(Token_Type type);

        /*
         * Checks if the current lexeme is any of the operator.
         *
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - true if current token is operator || false if not.
         * */
        bool check_if_operator();

        /*
         * Checks if the current lexeme is either a close bracket or comma or eof
         *
         *
         * @throws ParserExceptions - whenever an unexpected token is found.
         * @returns - true if current token is an end operator || false if not.
         * */
        bool check_if_end();
    public:

        /*
         * Starts the parsing of the program.
         *
         * @param filename - name of the file to parse
         * @catches ParserExceptions - whenever an unexpected token is found.
         * @return true when syntax tree is created || false when an exception is caught.
         * */
        bool start_parser(string filename);
        /*
         * Starts parsing single lines of code given from REPL input.
         *
         * @param lexer - lexer object to be used for parsing.
         * @catches ParserExceptions - whenever an unexpected token is found.
         * @return true when syntax tree is created || false when an exception is caught.
         * */
        bool start_parser(Lexer *lexer);

        /*
         * returns syntax tree.
         * */
        vector<AstStatementNode *>getSyntaxTree();
        ~Parser();
    };

}




#endif //CT_ASSIGNMENT_PARSER_H
