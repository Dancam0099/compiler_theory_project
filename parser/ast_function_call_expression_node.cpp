//
// Created by daniel on 3/29/18.
//

#include "ast_function_call_expression_node.h"

AstFunctionCallExpressionNode::~AstFunctionCallExpressionNode() {
    for (auto &i : func_expr) {
        delete i;
    }
}

void AstFunctionCallExpressionNode::accept(Visitor *visitor) {
    visitor->visit(this);
}

void AstFunctionCallStatement::accept(Visitor *visitor) {
    visitor->visit(this);
}
