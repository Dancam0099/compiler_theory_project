//
// Created by daniel on 3/28/18.
//

#include "ast_assignment_statement.h"

void AstAssignmentStatementNode::accept(Visitor *visitor) {
    visitor->visit(this);
}

AstAssignmentStatementNode::~AstAssignmentStatementNode() {
    delete value;
}
