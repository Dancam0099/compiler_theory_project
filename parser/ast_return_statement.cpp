//
// Created by daniel on 4/2/18.
//

#include "ast_return_statement.h"

AstReturnStatementNode::~AstReturnStatementNode() {
    delete returnexp;
}

void AstReturnStatementNode::accept(Visitor *visitor) {
    visitor->visit(this);
}
