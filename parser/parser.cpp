#include "parser.h"
#include "ast_binary_expression_node.h"
#include "ast_function_call_expression_node.h"
#include "Ast_Unary_Expression_node.h"
#include "ast_if_statement.h"
#include "ast_while_statement.h"
#include "ast_print_statement.h"
#include "ast_assignment_statement.h"
#include "ast_return_statement.h"
#include "../Exceptions/ParserExceptions.h"


bool parser::Parser::start_parser(string filename) {
    lex = new lexer::Lexer(filename);
    if(!lex->read_file())
        return false;
    xmlMaker = new XmlMakerVisitor();
    while(lex->tk.type != lexer::TOK_EOF){
        try{
            AstStatementNode *node =  ParseStatement();
            if(node != nullptr)
                statements.push_back(node);
        }catch (ParserExceptions &e){
            return false;
        }
    }
    xmlMaker->InitiateXML(statements);
    return true;
}


bool parser::Parser::start_parser(Lexer *lexer) {
    lex =lexer;
    while(lex->tk.type != lexer::TOK_EOF){
        try{
            AstStatementNode *node =  ParseStatement();
            if(node != nullptr)
                statements.push_back(node);
        }catch (ParserExceptions &e){
            return false;
        }
    }
    return true;
}

AstStatementNode *parser::Parser::ParseStatement() {
    AstStatementNode *node = nullptr;
    if(lex->tk.type == TOK_cSQBRACKET || lex->tk.type == TOK_SEMIC || lex->tk.value == "" || lex->tk.type == TOK_cBRACKET)
        lex->tokens_creator();
    switch (lex->tk.type){
        case lexer::TOK_FUNCDEF : node = ParseFunctionDecleration();
            break;
        case lexer::TOK_VAR : node = ParseVarDeclare();
            break;
        case TOK_IF: node = ParseIfStatement();
            break;
        case TOK_WHILE: node = ParseWhileStatement();
            break;
        case TOK_PRINT: node = ParsePrintStatement();
            break;
        case TOK_SET: node = ParseAssignmentStatement();
            break;
        case TOK_RETURN: node = ParseReturnStatement();
            break;
        case TOK_IDENTIFIER: node = ParseFunctioncall();
            break;
        case TOK_ERR: throw ParserExceptions("Unexpected token");
        default: break;
    }

    return node;
}



AstFunctionNode *parser::Parser::ParseFunctionDecleration() {
    auto *func = new AstFunctionNode();

    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_IDENTIFIER){
        delete func;
        throw ParserExceptions("Unknown token at function deceleration missing identifier");
    }

    func->identifier = lex->tk.value;

    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_oBRACKET){
        delete func;
        throw ParserExceptions("Unknown token at function deceleration missing open bracket");
    }

    lex ->tokens_creator();
    func->internal_parameters = ParseFormalParams(func->internal_parameters);

    if(lex->tk.type != lexer::TOK_cBRACKET){
        delete func;
        throw ParserExceptions("Unknown token at function deceleration missing closed bracket");
    }

    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_COLON){
        delete func;
        throw ParserExceptions("Unknown token at function deceleration missing colon");
    }

    lex ->tokens_creator();
    if(!checkIdentifierType(lex->tk.type)){
        delete func;
        throw ParserExceptions("Unknown token at function deceleration not valid type");
    }
    func->type = lex->tk.type;

    lex ->tokens_creator();
    if(lex->tk.type != TOK_oSQBRACKET){
        delete func;
        throw ParserExceptions("Unknown token at function deceleration no square bracket found");
    }

    func->block = ParseBlockStatement();
    return func;
}

vector<ASTFormalParams *> parser::Parser::ParseFormalParams(vector<ASTFormalParams *> params) {
    ASTFormalParams *node;
    int comma_count = 0;

    while(lex->tk.type != lexer::TOK_cBRACKET){
        node = new ASTFormalParams();
        if(lex->tk.type == lexer::TOK_cBRACKET){
            return params;
        }


        if(lex->tk.type != lexer::TOK_IDENTIFIER){
            delete node;
            throw ParserExceptions("Unknown token at function parameters 1");
        }
        node->identifier = lex->tk.value;

        lex ->tokens_creator();
        if(lex->tk.type != lexer::TOK_COLON){
            delete node;
            throw ParserExceptions("Unknown token at function parameters 2");
        }


        lex ->tokens_creator();
        if(!checkIdentifierType(lex->tk.type)) {
            delete node;
            throw ParserExceptions("Unknown token at function parameters 3");
        }

        node->type = lex->tk.type;
        lex ->tokens_creator();
        if(lex->tk.type == lexer::TOK_COMMA)
            comma_count +=1;
        else if(lex->tk.type == lexer::TOK_cBRACKET){
            params.push_back(node);
            return params;
        }
        else{
            delete node;
            throw ParserExceptions("Unknown token at function parameters 4");
        }
        params.push_back(node);
        lex ->tokens_creator();
    }
    if(params.size() != comma_count+1){
        delete node;
        throw ParserExceptions("Unknown token at function parameters incorrect amount of identifiers or commas");
    }
        return params;
}

AstVarDeclareStatement *parser::Parser::ParseVarDeclare() {
    auto *var = new AstVarDeclareStatement();

    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_IDENTIFIER){
        delete var;
        throw ParserExceptions("Unknown token at Variable Deceleration 1");
    }
    var->identifier = lex->tk.value;

    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_COLON){
        delete var;
        throw ParserExceptions("Unknown token at Variable Deceleration 2");
    }

    lex ->tokens_creator();
    if(!checkIdentifierType(lex->tk.type)){
        delete var;
        throw ParserExceptions("Unknown token at Variable Deceleration 3");
    }
    var->type = lex->tk.type;

    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_EQUALS){
        delete var;
        throw ParserExceptions("Unknown token at Variable Deceleration 4");
    }
    var->value = ParseExpression();
    if(lex->tk.type != TOK_SEMIC){
        delete var;
        throw ParserExceptions("Unknown token at Variable Deceleration 5");
    }

    return var;
}

AstExpressionNode *parser::Parser::ParseExpression() {
    AstExpressionNode *lhs = ParseSimpleExpression();
    AstExpressionNode *rhs = nullptr;
    string relop;

    if(!check_if_end() && !check_if_operator()){
        delete lhs,rhs;
        throw ParserExceptions("Unknown operator In parse Expression");
    }


    if(lex->tk.type == TOK_RELOP)
        relop = lex->tk.value;

    if(!check_if_end()){
        rhs = ParseExpression();
    }else
        return lhs;

    AstBinaryNode *node = new AstBinaryNode(lhs, rhs,relop);
    return node;

}

AstExpressionNode *parser::Parser::ParseSimpleExpression() {
    AstExpressionNode *lhs = ParseTerm();
    string operator_;
    AstExpressionNode *rhs = nullptr;

    if(!check_if_operator() && !check_if_end()){
        delete lhs,rhs;
        throw ParserExceptions("Unknown operator In parse simple expression");
    }

    if(lex->tk.type == TOK_ARITHMETICOP || lex->tk.type == TOK_UOR){
        operator_ = lex->tk.value;
        rhs = ParseSimpleExpression();
    }else
        return lhs;


    AstBinaryNode *node = new AstBinaryNode(lhs, rhs,operator_);
    return node;
}

AstExpressionNode *parser::Parser::ParseTerm() {
    AstExpressionNode *lhs = ParseFactor();
    AstExpressionNode *rhs = nullptr;

    string operator_;

    if(!check_if_operator() && !check_if_end()){
        delete lhs,rhs;
        throw ParserExceptions("Unknown operator In parse term"+operator_);
    }


    if(lex->tk.type == TOK_MULTIOP || lex->tk.type == TOK_AND){
        operator_ = lex->tk.value;
        rhs = ParseTerm();

    }else
        return lhs;



    AstBinaryNode *node = new AstBinaryNode(lhs, rhs,operator_);
    return node;
}

AstExpressionNode *parser::Parser::ParseFactor() {
    lex->tokens_creator();
    string value;
    string identifier;
    AstExpressionNode *lhs = nullptr;
    switch(lex->tk.type){
        case  TOK_NUM :
        case TOK_FLT :
        case TOK_TRUE:
        case TOK_FALSE:
        case TOK_STRLITERAL:
            value = lex->tk.value;
            lex->tokens_creator();
            break;

        case TOK_IDENTIFIER:
            identifier = lex->tk.value;
            lhs = ParseFunctionCall(lex->tk.value);

            if(lhs == nullptr){
                value = identifier;
            }else{
                lex->tokens_creator();
                return lhs;
            }
            break;

        case TOK_oBRACKET:
            lhs = ParseSubExpression();
            lex->tokens_creator();
            return lhs;


        case TOK_ARITHMETICOP:
            if(lex->tk.value == "-" or lex->tk.value == "not"){
                string temp = lex->tk.value;
                return new AstBinaryNode(ParseUnaryCall(),nullptr,temp);
            }

        default:
            return nullptr;
    }

    AstBinaryNode *node = new AstBinaryNode(lhs, nullptr,value);
    if(value == ""){
        delete node;
        throw ParserExceptions("unexpected token in factor");

    }
    return node;

}
AstExpressionNode *parser::Parser::ParseFunctionCall(string identifier) {
    auto func = new AstFunctionCallExpressionNode();
    lex->tokens_creator();
    int comma_count = 0;
    if(lex->tk.type != TOK_oBRACKET) {
        delete func;
        return nullptr;
    }
    func->identifier = identifier;

    while(lex->tk.type != TOK_cBRACKET && lex->tk.type != TOK_EOF) {
        func->func_expr.push_back(ParseExpression());

        if(lex->tk.type != TOK_cBRACKET && lex->tk.type != TOK_COMMA){
            delete func;
            throw ParserExceptions("extra or insufficient amount of parameter variables in function call");
        }

        if(lex->tk.type == TOK_COMMA)
            comma_count+=1;
    }

    if(lex->tk.type == TOK_EOF){
        delete func;
        throw ParserExceptions("end of file in function call");
    }

    if(comma_count+1 != func->func_expr.size()){
        delete func;
        throw ParserExceptions("extra or insufficient amount of parameter variables in function call");
    }

    return func;

}
AstExpressionNode *parser::Parser::ParseUnaryCall() {
    auto unexp = ParseFactor();
    return unexp;
}


AstExpressionNode *parser::Parser::ParseSubExpression() {
    AstExpressionNode *subexp = ParseExpression();
    if(lex->tk.type != TOK_cBRACKET){
        delete subexp;
        throw ParserExceptions("Unknown token at sub expression");
    }
    return subexp;
}

bool parser::Parser::check_if_operator() {
    switch (lex->tk.type){
        case TOK_ARITHMETICOP:
        case TOK_REAL:
        case TOK_FLT:
        case TOK_AND:
        case TOK_UOR:
        case TOK_NOT:
        case TOK_oBRACKET:
        case TOK_RELOP:
        case TOK_MULTIOP:
            return true;
        default:
            return false;
    }
}

bool parser::Parser::check_if_end() {
    switch (lex->tk.type){
        case TOK_SEMIC:
        case TOK_COMMA:
        case TOK_cBRACKET:
        case TOK_oSQBRACKET:
            return true;
        default:
            return false;
    }
}

AstBlockStatementNode *parser::Parser::ParseBlockStatement() {
    auto *statementNode = new AstBlockStatementNode();
    if(lex->tk.type != TOK_oSQBRACKET){
        delete statementNode;
        throw ParserExceptions("error no open square bracket found");
    }

    lex->tokens_creator();
    while(lex->tk.type != TOK_cSQBRACKET){
        AstStatementNode *node = ParseStatement();

        if(node != nullptr)
            statementNode->b_statements.push_back(node);
    }

    lex->tokens_creator();


    return statementNode;
}

AstStatementNode *parser::Parser::ParseIfStatement() {
    auto *ifnode = new AstIfStatementNode();
    lex->tokens_creator();
    if(lex->tk.type != TOK_oBRACKET){
        delete ifnode;
        throw ParserExceptions(" unknown tokens at If statement");
    }

    ifnode->ifcondition = ParseExpression();

    if(lex->tk.type != TOK_cBRACKET){
        delete ifnode;
        throw ParserExceptions(" unknown tokens at If statement");
    }

    lex->tokens_creator();
    if(lex->tk.type != TOK_oSQBRACKET){
        delete ifnode;
        throw ParserExceptions(" unknown tokens at If statement");
    }
    ifnode->if_statements = ParseBlockStatement();

    if(lex->tk.type == TOK_ELSE){
        lex->tokens_creator();
        ifnode->else_statements = ParseBlockStatement();
    }

    return ifnode;
}

AstStatementNode *parser::Parser::ParseWhileStatement() {
    auto *whilenode = new AstWhileStatementNode();
    lex->tokens_creator();
    if(lex->tk.type != TOK_oBRACKET){
        delete whilenode;
        throw ParserExceptions(" unknown tokens at while statement");
    }
    whilenode->params = ParseExpression();

    if(lex->tk.type != TOK_cBRACKET){
        delete whilenode;
        throw ParserExceptions(" unknown tokens at while statement");
    }

    lex->tokens_creator();
    if(lex->tk.type != TOK_oSQBRACKET){
        delete whilenode;
        throw ParserExceptions(" unknown tokens at while statement");
    }
    whilenode->while_statements = ParseBlockStatement();
    return whilenode;

}

AstStatementNode *parser::Parser::ParsePrintStatement() {
    auto *printStatementNode =  new AstPrintStatementNode();
    printStatementNode->print = ParseExpression();

    if(lex->tk.type != TOK_SEMIC){
        delete printStatementNode;
        throw ParserExceptions(" unknown tokens at print statement");
    }
    return printStatementNode;
}

AstStatementNode *parser::Parser::ParseAssignmentStatement() {
    auto set_var = new AstAssignmentStatementNode();
    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_IDENTIFIER){
        delete set_var;
        throw ParserExceptions(" unknown tokens at assignment statement");
    }
    set_var->identifier = lex->tk.value;

    lex ->tokens_creator();
    if(lex->tk.type != lexer::TOK_EQUALS){
        delete set_var;
        throw ParserExceptions(" unknown tokens at assignment statement");
    }
    set_var->value = ParseExpression();


    if(lex->tk.type != TOK_SEMIC){
        delete set_var;
        throw ParserExceptions(" unknown tokens at assignment statement");
    }
    return set_var;

}

AstStatementNode *parser::Parser::ParseReturnStatement() {
    auto returnstate = new AstReturnStatementNode();

    returnstate->returnexp = ParseExpression();
    if(lex->tk.type != TOK_SEMIC){
        delete returnstate;
        throw ParserExceptions(" unknown tokens at return statement");
    }
    return returnstate;
}

parser::Parser::~Parser() {
    delete lex;
    for (auto &statement : statements) {
        delete statement;
    }
}

bool parser::Parser::checkIdentifierType(Token_Type type) {
    switch(type){
        case TOK_INT:
        case TOK_STRLITERAL:
        case TOK_REAL:
        case TOK_BOOL:
        case TOK_STRING:
            return true;
        default:
            return false;
    }
}

vector<AstStatementNode *> parser::Parser::getSyntaxTree() {
    return statements;
}

AstStatementNode *parser::Parser::ParseFunctioncall() {
    auto *funccall = new AstFunctionCallStatement();

    funccall->functionCall = ParseFunctionCall(lex->tk.value);
    return funccall;
}

