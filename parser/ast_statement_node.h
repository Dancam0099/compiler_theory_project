#ifndef CT_ASSIGNMENT_AST_STATEMENT_NODE_H
#define CT_ASSIGNMENT_AST_STATEMENT_NODE_H

#include "astnode.h"
#include "../visitor/visitor.h"

class Visitor;
class AstStatementNode: public virtual ASTnode {
public:
    virtual void accept(Visitor *testy) = 0; //visitor virtual abstract

};
#endif //CT_ASSIGNMENT_AST_STATEMENT_NODE_H
