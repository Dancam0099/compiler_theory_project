//
// Created by daniel on 3/28/18.
//

#include "ast_var_declare_statement.h"

AstVarDeclareStatement::~AstVarDeclareStatement() {
    delete value;
}

void AstVarDeclareStatement::accept(Visitor *visitor) {
    visitor->visit(this);
}
