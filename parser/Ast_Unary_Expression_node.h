#ifndef CT_ASSIGNMENT_AST_UNARY_EXPRESSION_NODE_H
#define CT_ASSIGNMENT_AST_UNARY_EXPRESSION_NODE_H

#include "../Includer.h"
#include "ast_expression_node.h"

class PrintInfoVisitor;
class AstUnaryExpresionNode : public AstExpressionNode {
public:
    string Unary_op; // type of unary
    AstExpressionNode *un_exp; //unary expression
    ~AstUnaryExpresionNode();
    void accept(Visitor *visitor);
};


#endif //CT_ASSIGNMENT_AST_UNARY_EXPRESSION_NODE_H
