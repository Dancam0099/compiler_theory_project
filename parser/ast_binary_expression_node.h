//
// Created by daniel on 3/31/18.
//

#ifndef CT_ASSIGNMENT_AST_BINARY_EXPRESSION_NODE_H
#define CT_ASSIGNMENT_AST_BINARY_EXPRESSION_NODE_H


#include "ast_expression_node.h"
#include "../visitor/visitor.h"

class AstBinaryNode :public AstExpressionNode{


public:
    AstExpressionNode *LHS; // left hand side of expression
    AstExpressionNode *RHS; // right hand side of expression
    string operator_; // operator
    AstBinaryNode(AstExpressionNode *LHS,AstExpressionNode *RHS,string operator_); // initilzation of binary op
    ~AstBinaryNode(); // destructor
    void accept(Visitor *visitor); //visitor

};


#endif //CT_ASSIGNMENT_AST_BINARY_EXPRESSION_NODE_H
