//
// Created by daniel on 4/2/18.
//

#ifndef CT_ASSIGNMENT_AST_WHILE_STATEMENT_H
#define CT_ASSIGNMENT_AST_WHILE_STATEMENT_H


#include "ast_statement_node.h"
#include "ast_expression_node.h"
#include "ast_block_statement.h"

class AstWhileStatementNode : public AstStatementNode{
public:
    AstExpressionNode *params; // condition expression
    AstBlockStatementNode *while_statements; // while statements
    ~AstWhileStatementNode();
    void accept(Visitor *visitor);


};


#endif //CT_ASSIGNMENT_AST_WHILE_STATEMENT_H
