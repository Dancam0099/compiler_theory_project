//
// Created by daniel on 3/31/18.
//

#include "ast_binary_expression_node.h"
AstBinaryNode::AstBinaryNode(AstExpressionNode *LHS, AstExpressionNode *RHS, string operator_):LHS(LHS), RHS(RHS),operator_(operator_) {}

AstBinaryNode::~AstBinaryNode() {
    delete LHS;
    delete RHS;
}

void AstBinaryNode::accept(Visitor *visitor) {
    visitor->visit(this);
}


