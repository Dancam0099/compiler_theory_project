//
// Created by daniel on 4/2/18.
//

#include "ast_print_statement.h"

AstPrintStatementNode::~AstPrintStatementNode() {
    delete print;
}

void AstPrintStatementNode::accept(Visitor *visitor) {
    visitor->visit(this);
}
