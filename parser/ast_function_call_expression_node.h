#ifndef CT_ASSIGNMENT_AST_FUNCTION_CALL_EXPRESSION_NODE_H
#define CT_ASSIGNMENT_AST_FUNCTION_CALL_EXPRESSION_NODE_H


#include "ast_expression_node.h"
#include "ast_statement_node.h"
class AstFunctionCallStatement: public AstStatementNode{
public:
    void accept(Visitor *visitor);
    AstExpressionNode *functionCall;

};
class AstFunctionCallExpressionNode : public AstExpressionNode{
public:
    string identifier;
    vector<AstExpressionNode *> func_expr; // function call parameters
    ~AstFunctionCallExpressionNode(); //destructor
    void accept(Visitor *visitor); // visitor
};


#endif //CT_ASSIGNMENT_AST_FUNCTION_CALL_EXPRESSION_NODE_H
