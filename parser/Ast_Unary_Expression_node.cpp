#include "Ast_Unary_Expression_node.h"

AstUnaryExpresionNode::~AstUnaryExpresionNode() {
    delete un_exp;
}

void AstUnaryExpresionNode::accept(Visitor *visitor) {
    visitor->visit(this);
}
