#ifndef CT_ASSIGNMENT_AST_EXPRESSION_NODE_H
#define CT_ASSIGNMENT_AST_EXPRESSION_NODE_H


#include "astnode.h"
#include "../visitor/visitor.h"

class AstExpressionNode: public virtual ASTnode{
public:
    virtual void accept(Visitor *visitor) = 0; // visitor
};

#endif //CT_ASSIGNMENT_AST_EXPRESSION_NODE_H
