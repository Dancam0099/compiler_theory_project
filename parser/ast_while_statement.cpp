//
// Created by daniel on 4/2/18.
//

#include "ast_while_statement.h"

AstWhileStatementNode::~AstWhileStatementNode() {
    delete params;
    delete while_statements;
}

void AstWhileStatementNode::accept(Visitor *visitor) {
    visitor->visit(this);
}
