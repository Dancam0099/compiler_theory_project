#include "ast_block_statement.h"

AstBlockStatementNode::~AstBlockStatementNode() {
    for (auto &b_statement : b_statements) {
        delete b_statement;
    }
}

void AstBlockStatementNode::accept(Visitor *visitor) {
        visitor->visit(this);
}
