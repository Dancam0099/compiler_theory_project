//
// Created by daniel on 3/28/18.
//

#include "ast_funcDec.h"

AstFunctionNode::~AstFunctionNode() {
    delete block;
    for (auto &internal_parameter : internal_parameters) {
        delete internal_parameter;
    }

}

void AstFunctionNode::accept(Visitor *visitor) {
    visitor->visit(this);
}
