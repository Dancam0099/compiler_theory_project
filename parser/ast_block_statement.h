//
// Created by daniel on 4/2/18.
//

#ifndef CT_ASSIGNMENT_AST_BLOCK_STATEMENT_H
#define CT_ASSIGNMENT_AST_BLOCK_STATEMENT_H


#include "ast_statement_node.h"
#include "../visitor/visitor.h"

class AstBlockStatementNode {
public:
    vector<AstStatementNode *> b_statements; // list of statements
    void accept(Visitor *visitor); // visitor
    ~AstBlockStatementNode(); // destructor
};


#endif //CT_ASSIGNMENT_AST_BLOCK_STATEMENT_H
