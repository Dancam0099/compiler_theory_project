//
// Created by daniel on 4/4/18.
//

#ifndef CT_ASSIGNMENT_PARSEREXCEPTIONS_H
#define CT_ASSIGNMENT_PARSEREXCEPTIONS_H


#include <iostream>
#include <exception>
using namespace std;
class ParserExceptions : public exception{
public:
    ParserExceptions(string message);

};


#endif //CT_ASSIGNMENT_PARSEREXCEPTIONS_H
