//
// Created by daniel on 5/7/18.
//

#ifndef CT_ASSIGNMENT_SEMANTICEXCEPTIONS_H
#define CT_ASSIGNMENT_SEMANTICEXCEPTIONS_H

#include "../Includer.h"

class SemanticExceptions : public exception{
public:
    SemanticExceptions(string message){
        cout<<message<<endl;
    }
};


#endif //CT_ASSIGNMENT_SEMANTICEXCEPTIONS_H
